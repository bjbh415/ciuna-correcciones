<?php $_SESSION["title"] = movement ?>
<div class="box">
	<div class="box-tools">
		<div class="box-tool-left">
			<a href="<?=url_base?>home/dashboard"><?=dashboard?></a> <i class="fa fa-angle-right"></i> <a href="<?=url_base.routerCtrl?>"><?=((routerCtrl == "entry")? 'Entradas' : 'Asignaciones')?></a> <?=(action!="index")? "<i class='fa fa-angle-right'></i> ".((action=="add")? add : ((action=="edit")? edit : query ) ) : ''?>
		</div>
		<div class="box-tool-right"><i class="glyphicon glyphicon-minus"></i></div>
	</div>
	<div class="box-container">
		<?php if(action=="index"){ ?>
			<?=$dependencies['add']?>
			<table id="datatable" class="table table-striped table-bordered table-hover dataTable" width="100%">
                <thead><th><?=id?></th><th><?=(routerCtrl=='entry'? movement_name_entry : movement_name_assign)?></th><th><?=actions?></th></thead>
                <tfoot><th><?=id?></th><th><?=(routerCtrl=='entry'? movement_name_entry : movement_name_assign)?></th><th><?=actions?></th></tfoot>
            </table>
            <script>
	            $(document).ready( function () {
	                $('#datatable').dataTable(
		                {
		                	"language":{
		                    	"url": "<?=url_base?>third_party/datatables/language/es.json"
		                        },
	                        "processing": true,
	                        "serverSide": true,
	                        "ordering": false,
	                        "ajax": { url : "<?=url_base.routerCtrl?>/listt", type : "POST" },
	                        "columns": [
	                            { "data": "idmovement" },
	                            { "data": "name" },
	                            { "data": "btn" }
	                        ]
	                    }
	                ); 
	            });
	        </script>
		<?php }else{ ?>
			<?=(action!="query")? "<form action='".url_base.routerCtrl."/".action."/".$d["idmovement"]."' method='POST' class='form-horizontal'>" : "<div class='form-horizontal'>" ?>
				<input type="hidden" name="event" id="event">
				<input type="hidden" name="type_movement" id="type_movement" value='<?=(routerCtrl=='entry'? 1 : 2)?>'>
				<?php
					if(action!="add")
						echo "<div class='form-group'>
							<label class='col-md-2 text-right'>".id.":</label>
							<div class='col-md-3'>
								<input type='text' name='idmovement' id='idmovement' value='".$d["idmovement"]."' class='width-full' disabled data-toggle='tooltip' title='".id_title."'>
							</div>
						</div>";
				?>
				<?php if(routerCtrl=='entry'){ ?>
					<div class="form-group">
						<label class="col-md-2 text-right">Responsable:</label>
						<div class="col-md-4">
							<input type="text" name="responsable" id="responsable" value="<?=(action!="query")?$_SESSION["pename_one"].' '.$_SESSION["pelast_name_one"].' '.$_SESSION["cname"]:$d["responsable"] ?>" aajs="required" class="width-full" data-toggle="tooltip" title="responsable " placeholder="" autocomplete="off">
						</div>
						<label class="col-md-2 text-right">Fecha/Hora de Entrada:</label>
						<div class="col-md-2">
							<input type="text" name="date_register" id="date_register" value="<?=(action!="query")? date("Y-m-d H:i:s") : $d["date_register"]?>" aajs="required" class="width-full datetimepickerx" data-toggle="tooltip" title="Fecha/Hora de Entrada " placeholder="" autocomplete="off">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-2 text-right"><?=movement_idprovider?>:</label>
						<div class="col-md-4">
							<input type="text" id="provider" value="<?=$d["full_provider"]?>" aajs="searchajax{<?=url_base?>provider/search,this},required" class="width-full" data-toggle="tooltip" title="<?=movement_idprovider_title?>" placeholder="<?=movement_idprovider_placeholder?>" autocomplete="off">
							<input type="hidden" name="idprovider" id="idprovider" value="<?=$d["idprovider"]?>" aajs="required">
						</div>
						<label class="col-md-2 text-right"><?=movement_idtype_entry?>:</label>
						<div class="col-md-2">
							<select name="idtype_entry" id="idtype_entry" class="width-full" aajs="required" data-toggle="tooltip" title="<?=movement_idtype_entry_title?>">
								<?php
									foreach ($dependencies["type_entrys"] as $type_entry){
										echo "<option value='".$type_entry["idtype_entry"]."' ".(($d["idtype_entry"]==$type_entry["idtype_entry"])? 'selected' : '').">".$type_entry["name"]."</option>";
									}
								?>
							</select>
						</div>
					</div>
					<!--<div class="form-group">
						<label class="col-md-2 text-right">Nro. Entrada:</label>
						<div class="col-md-4">
							<input type="text" name="nro_entry" id="nro_entry" value="<?=$d["nro_entry"]?>" class="width-full" data-toggle="tooltip" title="Nro de entrada" placeholder="" autocomplete="off">
						</div>
						<label class="col-md-2 text-right">Fecha Recepcion:</label>
						<div class="col-md-2">
							<input type="text" name="date_entry" id="date_entry" value="<?=$d["entry_date"]?>" class="width-full datepickerx" data-toggle="tooltip" title="Fecha de recepcion" placeholder="" autocomplete="off">
						</div>
					</div>-->
					<div class="form-group">
<!--FECHA DE FACTURA-->
						<label class="col-md-2 text-right">Fecha de Factura:</label>
						<div class="col-md-4">
							<input type="text" name="date_billing_request" id="date_billing_request" value="<?=$d["date_billing_request"]?>" aajs="required,change{isValidBillingDateRequest();}" class="width-full datepickerx" data-toggle="tooltip" title="Fecha de factura" placeholder="" autocomplete="off">
						</div>
<!--//FECHA DE FACTURA-->
						<label class="col-md-2 text-right">Nro. de Factura:</label>
						<div class="col-md-2">
							<input type="text" name="number_billing_request" id="number_billing_request" value="<?=$d["number_billing_request"]?>" class="width-full" data-toggle="tooltip" title="" placeholder="" autocomplete="off">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 text-right">Observacion:</label>
						<div class="col-md-8">
							<textarea name="observation" id="observation" cols="30" rows="2" class="width-full"><?=$d["observation"]?></textarea>
						</div>
					</div>
				<?php }else{ ?>
					<!--<div class="form-group">
						<label class="col-md-2 text-right">Nro. de Asignacion:</label>
						<div class="col-md-4">
							<input type="text" name="nro_entry" id="nro_entry" value="<?=$d["nro_entry"]?>" class="width-full" data-toggle="tooltip" title="Nro de entrada" placeholder="" autocomplete="off">
						</div>
						<label class="col-md-2 text-right">Fecha de Asignacion:</label>
						<div class="col-md-2">
							<input type="text" name="date_entry" id="date_entry" value="<?=$d["entry_date"]?>" class="width-full datepickerx" data-toggle="tooltip" title="Fecha de recepcion" placeholder="" autocomplete="off">
						</div>
					</div>-->
					<div class="form-group">
						<label class="col-md-2 text-right">Nro. Solicitud:</label>
						<div class="col-md-4">
							<input type="text" name="request_number" id="request_number" value="<?=$d["code"]?>" class="width-full" aajs="change{searchRequest()},required" data-toggle="tooltip" title="" placeholder="" autocomplete="off">
						</div>
						<label class="col-md-2 text-right">Fecha de Solicitud:</label>
						<div class="col-md-2">
							<input type="text" name="request_date" id="request_date" value="<?=$d["date_created"]?>" class="width-full" aajs="required" data-toggle="tooltip" title="Fecha de Solicitud" placeholder="" autocomplete="off">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 text-right"><?=movement_iddepartament?>:</label>
						<div class="col-md-3">
							<select name="iddepartament" id="iddepartament" class="width-full" aajs="required" data-toggle="tooltip" title="<?=movement_iddepartament_title?>">
								<?php
									foreach ($dependencies["departaments"] as $departament){
										echo "<option value='".$departament["iddepartament"]."' ".(($d["iddepartament"]==$departament["iddepartament"])? 'selected' : '').">".$departament["name"]."</option>";
									}
								?>
							</select>
						</div>
						<label class="col-md-2 text-right">Fecha/Hora Despacho </label>
						<div class="col-md-3">
							<?php if(action!='query'): ?>
								<p><?= date("Y-m-d H:i:s") ?></p>
								<input type="hidden" name="date_register" id="date_register" value="<?= date("Y-m-d H:i:s") ?>">
							<?php else: ?>
								<p><?= $d['date_register'] ?></p>
							<?php endif; ?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 text-right">Encargado de Solicitud:</label>
						<div class="col-md-6">
							<p class="applicant"><?=(action!='query')?'_________________________________':$d['applicant']?></p>
							<input type="hidden" name="applicant" class="applicant">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 text-right">Observacion:</label>
						<div class="col-md-8">
							<textarea name="observation" id="observation" cols="30" rows="2" class="width-full"><?=$d["observation"]?></textarea>
						</div>
					</div>
				<?php } ?>				
				<div class="row">
					<div class="col-md-10 col-md-offset-1">
						<?php if(routerCtrl=='entry'):?>
						<table id="datatable" class="table table-striped table-bordered table-hover dataTable" width="100%">
							<thead>
								<th><?=movement_article?></th>
								<th>Unidad de medida</th>
								<th>Tipo de Artículo</th>
								<th><?=movement_amount?></th>
								<th><?=add?></th>
							</thead>
							<tbody id="bodyTable">
								<tr>
									<td>
										<select id="article" onchange="search();" class="width-full" data-toggle="tooltip" title="<?=movement_idtype_entry_title?>">
											<option value="0">Seleccione un articulo</option>
											<?php
												foreach ($dependencies["articles"] as $article){
													if(action=="add"){
														echo "<option value='".$article["idarticle"]."' >".$article["name"]."</option>";
													}else{
														for($i=0;$i<count($dependencies["movement_articles"]);$i++){
															if($dependencies["movement_articles"][$i]["idarticle"]!=$article["idarticle"]){
																echo "<option value='".$article["idarticle"]."' >".$article["name"]."</option>";
															}
														}
													}
												}
											?>
										</select>
									</td>
									<td id="measurement">
										
									</td>
									<td id="type_article">
										
									</td>
									<td>
										<input type="text" id="amount" class="width-full" data-toggle="tooltip" title="<?=movement_idprovider_title?>" placeholder="<?=movement_idprovider_placeholder?>" autocomplete="off">
									</td>
									<td>
										<button type="button"class="btn1" id="add" ><i class="fa fa-plus"></i></button>
									</td>
								</tr>
								<?php
									foreach($dependencies["movement_articles"] as $key =>  $ma){
										echo"<tr>
											<td>".$ma["name"]."<input type='hidden' name='idarticle[]' id='idarticle-".$key."' value='".$ma["idarticle"]."' data-text='".$ma["name"]."'></td>
											<td>".$ma["code"]."<input type='hidden' name='code[]' id='codex' value='".$ma["code"]."'></td>
											<td></td>
											<td>".$ma["amount"]."<input type='hidden' name='amount[]' value='".$ma["amount"]."'></td>
											<td><button type='button' id='".$key."' class='btn1' onclick='remove(this);'><i class='fa fa-minus'></i></button></td>
										</tr>";
									}
								?>
							</tbody>
						</table>

						<?php else: ?>
						
						<table id="datatable" class="table table-striped table-bordered table-hover dataTable" width="100%">
							<thead>
								<th><?=movement_article?></th>
								<th><?=movement_amount?></th>
							</thead>
							<tbody id="bodyTable">
								<tr>
									<td>
										<select id="article" onchange="search();" class="width-full" data-toggle="tooltip" title="<?=movement_idtype_entry_title?>">
											<option value="0">Seleccione un articulo</option>
											<?php
												foreach ($dependencies["articles"] as $article){
													echo "<option value='".$article["idarticle"]."' >".$article["name"]."</option>";
												}
											?>
										</select>
									</td>
									<td>
										<input type="text" id="amount" class="width-full" data-toggle="tooltip" title="<?=movement_idprovider_title?>" placeholder="<?=movement_idprovider_placeholder?>" autocomplete="off">
									</td>
								</tr>
								<?php
									foreach($dependencies["movement_articles"] as $key =>  $ma){
										echo"<tr>
											<td>".$ma["name"]."<input type='hidden' name='idarticle[]' id='idarticle-".$key."' value='".$ma["idarticle"]."' data-text='".$ma["name"]."'></td>
											<td>".$ma["amount"]."<input type='hidden' name='amount[]' value='".$ma["amount"]."'></td>
										</tr>";
									}
								?>
							</tbody>
						</table>
						<?php endif; ?>
					</div>
				</div>
				<?php
					if(action!="query")
						echo"<div class='form-group'>
							<div class='col-md-2 col-md-offset-5'>
								<button class='btn1' aajs='send'>".save."</button>
							</div>
						</div>";
				?>
			<?=(action!="query")? "</form>" :'</div>' ?>
		<?php } ?>
	</div>
</div>
<script>
var cont = 1000;
function isValidAmount (elemt){
	elemt = $(elemt);
	var amount = Number(elemt.val());
	var totalAmount = Number(elemt.data("amount"));
	var x = {};
	if(amount > totalAmount)x = {ok:false, message: 'La cantidad de entrada no puede ser mayor a: ' + totalAmount};
	else if(amount < 0) x = {ok:false, message: 'La cantidad no puede ser menor a cero'};
	else if(isNaN(amount)) x = {ok:false, message: 'La cantidad debe ser numerica'};
	else x = {ok:true};

	if(!x.ok) {
		elemt.val(null);
		toastr.error('Error: ' + x.message,'',{progressBar:true});
		return false;
	}
}

$("#add").click(function(){
	var measurement = $("#measurement").text();
	var type_article = $("#type_article").text();
	var articleVal = $("#article").val();
	var articleText = $("#article option[value='"+articleVal+"']").text();
	var amount = $("#amount").val();
	if(articleVal=="0"){
		toastr.error('<?=movement_article_add_error?>','',{progressBar:true});
		return false;
	}
	if(amount==""){
		toastr.error('<?=movement_amount_add_error?>','',{progressBar:true});
		return false;
	}
	/*if(code==""){
		code="<?=movement_code_no?>";
	}*/
	$("#article option[value='"+$("#article").val()+"']").remove();
	var html = `<tr>
					<td>`+articleText+`<input type="hidden" name="idarticle[]" id="idarticle-`+cont+`" value="`+articleVal+`" data-text="`+articleText+`"></td>
					<td>`+measurement+`</td>
					<td>`+type_article+`</td>
					<td>`+amount+`<input type="hidden" name="amount[]" value="`+amount+`"></td>
					<td><button type="button" id="`+cont+`" class="btn1" onclick="remove(this);"><i class="fa fa-minus"></i></button></td>
				</tr>`;
	$("#bodyTable").append(html);
	cont++;
});
function remove(_this){
	var val = $("#idarticle-"+_this.id).val();
	var text = $("#idarticle-"+_this.id).attr("data-text");
	console.log(val+" "+text);
	$("#article").append("<option value='"+val+"'>"+text+"</option>");
	$(_this).parent().parent().remove();
}
function search(){
	$.post("<?=url_base?>article/search",{value:$("#article").val()},function(datas){
		var data = $.parseJSON(datas);
		console.log(data);
		if(data!=null){
			$("#measurement").text(data[0][0]);
			$("#type_article").text(data[0][1]);
		}else{
			$("#measurement").text("");
			$("#type_article").text("");
		}

	});
}

//CARGAR AUTOMATICAMENTE LOS DATOS RELACIONADOS CON EL NUMERO DE SOLICITUD
function searchRequest(){
	$.post("<?=url_base?>add-request/search",{value:$("#request_number").val()},function(datas){	
		console.log(datas);
		var data = $.parseJSON(datas);
		var thedata = (data[0]!=null)?
		{reqDate: data[0].date_created, departament: data[0].iddepartament, reqArts: data.requested_articles, code: data.code, applicant: data[0].applicant}
		:thedata = {reqDate: null, departament: null, reqArts: null, code: null, applicant: null};
		
		$("#request_date").val(thedata.reqDate);
		$("#iddepartament option[value="+(thedata.departament)+"]").prop('selected', true);
		$("#datatable thead").html('<tr><th>Articulos</th><th>Cantidad</th></tr>');
		$("p.applicant").text(thedata.applicant); $("input.applicant").val(thedata.applicant);
		var tbody = $("#datatable tbody");tbody.html('');
		thedata.reqArts.forEach(function(art){
			tbody.append(`
				<tr>
					<td>${art.name}</td>
					<input type="hidden" name="idarticle[]" value="${art.idarticle}">
					<td><input type="text" name="amount[]" class="width-full" onblur="isValidAmount(this)" value="${art.amount}" aajs="required" data-amount="${art.totalamount}" data-toggle="tooltip" title="<?=movement_idprovider_title?>" placeholder="<?=movement_idprovider_placeholder?>" autocomplete="off"></td>
				</tr>`
			);
		});

	});
}

//NO PERMITE FECHAS DE ENTRADAS INFERIOR A 7 DIAS NI POSTERIOR AL DIA ACTUAL
function isValidBillingDateRequest(){
	var elemtDate = $('#date_billing_request');
	var billingDateReq = (elemtDate.val()).split('-');
	var currentDate = ('<?= date("Y-m-d")?>').split('-');
	billingDateReq = new Date(billingDateReq[0], billingDateReq[1], billingDateReq[2]);
	currentDate = new Date(currentDate[0], currentDate[1], currentDate[2]);
	const SEVEN_DAYS = 7 * 24 * 60 * 60 * 1000; //d * h * min * sg * ms
	const DIFF_DATES = currentDate.getTime() - billingDateReq.getTime();
	if(currentDate >= billingDateReq){
		if(DIFF_DATES > SEVEN_DAYS) {
			elemtDate.val(null);
			toastr.error('Error: No se permiten facturas con más de 7 días de retraso','',{progressBar:true})
		}
	}else{
		elemtDate.val(null);
		toastr.error('Error: La fecha de entrada no puede ser posterior a hoy (<?= date("Y-m-d")?>)','',{progressBar:true})
	}
}
</script>