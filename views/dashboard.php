<?php $_SESSION["title"] = '' ?>
<div class="row">
	<div class="col-md-12 page-header">
		<figure class="text-center">
			<img class="img-thumbnail" src="<?= url_base ?>img/una-header.jpeg" style="width:100%"/>
		</figure>	
	
		<div id="myCarousel" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
				<li data-target="#myCarousel" data-slide-to="1"></li>
			</ol>
			<!-- Wrapper for slides -->
			<div class="carousel-inner">
				<div class="item active">

					<div class="container">
						<div class="jumbotron">
						<h2>MISIÓN</h2>
						<p class="text-justify">
						La Universidad Nacional Abierta es una institución venezolana, oficial y experimental, organizada como un sistema de educación abierta y a distancia de alcance nacional y proyección internacional, dirigida a democratizar y masificar el acceso a una educación permanente de calidad y comprometida con el desarrollo del país a diversas poblaciones que por limitaciones de variado origen no han podido ingresar o continuar en el subsistema de educación superior y distintos sectores de la sociedad que requieren del servicio educativo.
						Para el logro de sus propósitos, la Universidad emplea diferentes estrategias propias de la modalidad de educación a distancia, la investigación como una práctica institucionalizada así como variadas formas de articulación interinstitucional.
						</p>
						</div>
					</div>

				</div>

				<div class="item">

					<div class="container">
						<div class="jumbotron">
						<h2>VISIÓN</h2>
						<p class="text-justify">
						La Universidad Nacional Abierta será una institución de referencia nacional e internacional, en educación permanente, abierta y a distancia, reconocida por los siguientes rasgos:
						<ul>
							<li>Clara vocación democratizadora de la educación.</li>
							<li>Abierta en espacio y tiempo para los demandantes del servicio educativo.</li>
							<li>Formadora de ciudadanos actualizados, emprendedores, críticos y con conciencia de participación ciudadana.</li>
							<li>Rectora de la Educación a Distancia en el país.</li>
							<li>Flexible y desburocratizada en su organización y funcionamiento.</li>
							<li>Calidad integral del servicio educativo que presta.</li>
							<li>Gran formadora de las poblaciones tradicionalmente excluidas de la educación.</li>
							<li>Gran capacitadora de los empleados al servicio del Estado venezolano en instituciones de alcance nacional.</li>
						</ul>
						</p>
						</div>
					</div>

				</div>

			</div>

			<!-- Left and right controls -->
			<a class="left carousel-control" href="#myCarousel" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="right carousel-control" href="#myCarousel" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>

	</div>
	<div class="col-md-6">
		<div class="box">
			<div class="box-tools">
				<div class="box-tool-left">
					<a href="<?=url_base?>home/dashboard"><?=dashboard_note?></a>
				</div>
				<div class="box-tool-right"><i class="glyphicon glyphicon-minus"></i></div>
			</div>
			<div class="box-container">
				<form action="<?=url_base?>user/note" method="POST" class='form-horizontal'>
					<div class="form-group">
						<label class="col-md-12"><?=dashboard_note?>:
							<textarea name="note" id="note" class="width-full" style="height: 80px;resize: none;font-weight: normal;"><?=$dependencies["note"][0]?></textarea>
						</label>
					</div>
					<div class="form-group">
						<div class='col-md-2 col-md-offset-5'>
							<button class='btn1'><?=save?></button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="box">
			<div class="box-tools">
				<div class="box-tool-left">
					<a href="<?=url_base?>home/dashboard"><?=ameliacms_noticies?></a>
				</div>
				<div class="box-tool-right"><i class="glyphicon glyphicon-minus"></i></div>
			</div>
			<div class="box-container api_news"></div>
		</div>
	</div>
</div>
<script>
	$.get("<?=url_api?>api/news",function(datas){ 
		var data = $.parseJSON(datas);
		var html = "";
		for(var d in data){
			html+="<div class='api_news_title'><a href='"+data[d]["url"]+"'>"+data[d]["title"]+"</a></div>";
			html+="<div class='api_news_date'>"+data[d]["date_created"]+"</div>";
			html+="<div class='api_news_content'>"+data[d]["content"]+"</div>";
		}
		$(".api_news").html(html);
	});
</script>
<div class="row">
	<div class="col-md-12">	
		<div class="box">
				<div class="box-tools">
				<div class="box-tool-left">
					<a href="<?=url_base?>home/dashboard"><?=dashboard?></a>
				</div>
				<div class="box-tool-right"><i class="glyphicon glyphicon-minus"></i></div>
			</div>
			<div class="box-container"></div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-6">
		<div class="box">
			<div class="box-tools">
				<div class="box-tool-left">
					<a href="<?=url_base?>home/dashboard"><?=dashboard?></a>
				</div>
				<div class="box-tool-right"><i class="glyphicon glyphicon-minus"></i></div>
			</div>
			<div class="box-container"></div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="box">
			<div class="box-tools">
				<div class="box-tool-left">
					<a href="<?=url_base?>home/dashboard"><?=dashboard?></a>
				</div>
				<div class="box-tool-right"><i class="glyphicon glyphicon-minus"></i></div>
			</div>
			<div class="box-container"></div>
		</div>
	</div>
</div>
