<?php $_SESSION["title"] = type_entry ?>
<div class="box">
	<div class="box-tools">
		<div class="box-tool-left">
			<a href="<?=url_base?>home/dashboard"><?=dashboard?></a> <i class="fa fa-angle-right"></i> <a href="<?=url_base.routerCtrl?>"><?=type_entry?></a> <?=(action!="index")? "<i class='fa fa-angle-right'></i> ".((action=="add")? add : ((action=="edit")? edit : query ) ) : ''?>
		</div>
		<div class="box-tool-right"><i class="glyphicon glyphicon-minus"></i></div>
	</div>
	<div class="box-container">
		<?php if(action=="index"){ ?>
			<?=$dependencies['add']?>
			<table id="datatable" class="table table-striped table-bordered table-hover dataTable" width="100%">
                <thead><th><?=id?></th><th><?=type_entry_name?></th><th><?=actions?></th></thead>
                <tfoot><th><?=id?></th><th><?=type_entry_name?></th><th><?=actions?></th></tfoot>
            </table>
            <script>
	            $(document).ready( function () {
	                $('#datatable').dataTable(
		                {
		                	"language":{
		                    	"url": "<?=url_base?>third_party/datatables/language/es.json"
		                        },
	                        "processing": true,
	                        "serverSide": true,
	                        "ordering": false,
	                        "ajax": { url : "<?=url_base.routerCtrl?>/listt", type : "POST" },
	                        "columns": [
	                            { "data": "idtype_entry" },
	                            { "data": "name" },
	                            { "data": "btn" }
	                        ]
	                    }
	                ); 
	            });
	        </script>
		<?php }else{ ?>
			<?=(action!="query")? "<form action='".url_base.routerCtrl."/".action."/".$type_entry["idtype_entry"]."' method='POST' class='form-horizontal'>" : "<div class='form-horizontal'>" ?>
				<input type="hidden" name="event" id="event">
				<?php
					if(action!="add")
						echo "<div class='form-group'>
							<label class='col-md-2 text-right'>".id.":</label>
							<div class='col-md-3'>
								<input type='text' name='idtype_entry' id='idtype_entry' value='".$type_entry["idtype_entry"]."' class='width-full' disabled data-toggle='tooltip' title='".id_title."'>
							</div>
						</div>";
				?>
				<div class="form-group">
					<label class="col-md-2 text-right"><?=type_entry_name?>:</label>
					<div class="col-md-3">
						<input type="text" name="name" id="name" value="<?=$type_entry["name"]?>" aajs="required" class="width-full" <?=(action=="query")?'disabled':''?> data-toggle="tooltip" title="<?=type_entry_name_title?>" placeholder="<?=type_entry_name_placeholder?>">
					</div>
				</div>
				<?php
					if(action!="query")
						echo"<div class='form-group'>
							<div class='col-md-2 col-md-offset-5'>
								<button class='btn1' aajs='send'>".save."</button>
							</div>
						</div>";
				?>
			<?=(action!="query")? "</form>" :'</div>' ?>
		<?php } ?>
	</div>
</div>

<?php if(action=="query"): ?>

<div class="box">
	<div class="box-tools">
		<div class="box-tool-left">
			<h4><?= "Entradas por: <u>".$type_entry["name"]."</u>" ?></h4>
		</div>
		<!-- <div class="box-tool-right"><i class="glyphicon glyphicon-minus"></i></div> -->
	</div>
	<div class="box-container">
		<?php if(count($entrys) > 0): ?>
		<table class="table table-striped table-bordered table-condensed table-responsive">
			<tr>
				<th>ID</th>
				<th>Descripción</th>
				<th>Status</th>
			</tr>
			<?php foreach($entrys as $entry): ?>
			<tr>
				<td><?= $entry['idmovement'] ?></td>
				<td><?= $entry['name'] ?></td>
				<td><?= ($entry['status'] == 1)?'Activo':'Inactivo'; ?></td>
			</tr>
			<?php endforeach; ?>
		</table>
		<?php else: ?>
			<h1 class="text-center text-muted">No hay entradas de este tipo</h1>
		<?php endif; ?>
	</div>
</div>

<?php endif; ?>
