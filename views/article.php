<?php $_SESSION["title"] = article ?>
<div class="box">
	<div class="box-tools">
		<div class="box-tool-left">
			<a href="<?=url_base?>home/dashboard"><?=dashboard?></a> <i class="fa fa-angle-right"></i> <a href="<?=url_base.routerCtrl?>"><?=article?></a> <?=(action!="index")? "<i class='fa fa-angle-right'></i> ".((action=="add")? add : ((action=="edit")? edit : query ) ) : ''?>
		</div>
		<div class="box-tool-right"><i class="glyphicon glyphicon-minus"></i></div>
	</div>
	<div class="box-container">
		<?php if(action=="index"){ ?>
			<?=$dependencies['add']?>
			<table id="datatable" class="table table-striped table-bordered table-hover dataTable" width="100%">
                <thead><th><?=id?></th><th><?=article_name?></th><th>Tipo de Artículo</th><th><?=actions?></th></thead>
                <tfoot><th><?=id?></th><th><?=article_name?></th><th>Tipo de Artículo</th><th><?=actions?></th></tfoot>
            </table>
            <script>
	            $(document).ready( function () {
	                $('#datatable').dataTable(
		                {
		                	"language":{
		                    	"url": "<?=url_base?>third_party/datatables/language/es.json"
		                        },
	                        "processing": true,
	                        "serverSide": true,
	                        "ordering": false,
	                        "ajax": { url : "<?=url_base.routerCtrl?>/listt", type : "POST" },
	                        "columns": [
	                            { "data": "idarticle" },
	                            { "data": "name" },
	                            { "data": "type_article" },
	                            { "data": "btn" }
	                        ]
	                    }
	                ); 
	            });
	        </script>
		<?php }else{ ?>
			<?=(action!="query")? "<form action='".url_base.routerCtrl."/".action."/".$d["idarticle"]."' method='POST' class='form-horizontal'>" : "<div class='form-horizontal'>" ?>
				<input type="hidden" name="event" id="event">
				<?php
					if(action!="add")
						echo "<div class='form-group'>
							<label class='col-md-2 text-right'>".id.":</label>
							<div class='col-md-3'>
								<input type='text' name='idarticle' id='idarticle' value='".$d["idarticle"]."' class='width-full' disabled data-toggle='tooltip' title='".id_title."'>
							</div>
						</div>";
				?>
				<div class="form-group">
					<label class="col-md-2 text-right">Tipo de Artículo:</label>
					<div class="col-md-3">
						<input type="text" id="type_article" value="<?=$d["type_article"]?>" aajs="searchajax{<?=url_base?>type_article/search,this},required" class="width-full" data-toggle="tooltip" title="<?=article_idtype_article_title?>" placeholder="<?=article_idtype_article_placeholder?>" autocomplete="off">
						<input type="hidden" name="idtype_article" id="idtype_article" value="<?=$d["idtype_article"]?>" aajs="required">
					</div>
				</div>
				<!---->
				<div class="form-group">
					<label class="col-md-2 text-right"><?=article_idmodel?>:</label>
					<div class="col-md-3">
						<input type="text" id="model" value="<?=$d["model"]?>" aajs="searchajax{<?=url_base?>model/search,this},required" class="width-full" data-toggle="tooltip" title="<?=article_idmodel_title?>" placeholder="<?=article_idmodel_placeholder?>" autocomplete="off">
						<input type="hidden" name="idmodel" id="idmodel" value="<?=$d["idmodel"]?>" aajs="required">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 text-right"><?=article_name?>:</label>
					<div class="col-md-3">
						<input type="text" name="name" id="name" value="<?=$d["name"]?>" aajs="required,blur{exist();}" class="width-full" <?=(action=="query")?'disabled':''?> data-toggle="tooltip" title="<?=article_name_title?>" placeholder="<?=article_name_placeholder?>">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 text-right"><?=article_amount?>:</label>
					<div class="col-md-3">
						<input type="text" name="amount" id="amount" value="<?=$d["amount"]?>" aajs="required" class="width-full" <?=(action=="query")?'disabled':''?> data-toggle="tooltip" title="<?=article_amount_title?>" placeholder="<?=article_amount_placeholder?>" disabled>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 text-right">Unidad de medida:</label>
					<div class="col-md-3">
						<select name="idmeasurement" id="idmeasurement" class="width-full" data-toggle="tooltip" aajs="required">
							<?php
								foreach ($dependencies["measurements"] as $m){
									echo "<option value='".$m["idmeasurement"]."' ".(($d["idmeasurement"]==$m["idmeasurement"])? 'selected' : '').">".$m["name"]."</option>";
								}
							?>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 text-right"><?=article_stockmin?>:</label>
					<div class="col-md-3">
						<input type="text" name="stockmin" id="stockmin" value="<?=$d["stockmin"]?>" aajs="required" class="width-full" <?=(action=="query")?'disabled':''?> data-toggle="tooltip" title="<?=article_stockmin_title?>" placeholder="<?=article_stockmin_placeholder?>">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 text-right"><?=article_stockmax?>:</label>
					<div class="col-md-3">
						<input type="text" name="stockmax" id="stockmax" value="<?=$d["stockmax"]?>" aajs="required" class="width-full" <?=(action=="query")?'disabled':''?> data-toggle="tooltip" title="<?=article_stockmax_title?>" placeholder="<?=article_stockmax_placeholder?>">
					</div>
				</div>
				<!---->
				<?php
					if(action!="query")
						echo"<div class='form-group'>
							<div class='col-md-2 col-md-offset-5'>
								<button class='btn1' aajs='click{isValidStock()},send'>".save."</button>
							</div>
						</div>";
				?>
			<?=(action!="query")? "</form>" :'</div>' ?>
		<?php } ?>
	</div>
</div>
<script>
	function exist(){
		var field = document.getElementById("name");
		$.post("<?=url_base?>article/exist",{value:field.value},function(data){
			var d = $.parseJSON(data);
			if(d[0]["namex"] == field.value){
				toastr.error('Registro existente','',{progressBar:true})
				field.value="";
			}
		});
	}
</script>

<script>
	function isValidStock(){
		var fieldsmin = document.getElementById("stockmin")
		var fieldsmax = document.getElementById("stockmax");
		var valid = {ok:true};

		if(!isNaN(fieldsmin.value) && !isNaN(fieldsmax.value)){
			if(fieldsmin.value < 0 || fieldsmax.value < 0)
				valid = {ok:false, err:'Números Negativos en Stock Mínimo y/o Stock Máximo'};	
			else if(fieldsmin.value > fieldsmax.value)
				valid = {ok:false, err:'Stock Mínimo no puede ser mayor que Stock Máximo'};
		}else
			valid = {ok:false, err:'Valores invalidos en Stock Mínimo y/o Stock Máximo. Sólo Números'};		

		if(!valid.ok){
			toastr.error('Error: '+valid.err,'',{progressBar:true})
			fieldsmin.value = null;
			fieldsmax.value = null;
			return false;
		}
		return true;		
	}
</script>

