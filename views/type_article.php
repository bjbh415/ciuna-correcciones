<?php $_SESSION["title"] = type_article ?>
<div class="box">
	<div class="box-tools">
		<div class="box-tool-left">
			<a href="<?=url_base?>home/dashboard"><?=dashboard?></a> <i class="fa fa-angle-right"></i> <a href="<?=url_base.routerCtrl?>"><?=type_article?></a> <?=(action!="index")? "<i class='fa fa-angle-right'></i> ".((action=="add")? add : ((action=="edit")? edit : query ) ) : ''?>
		</div>
		<div class="box-tool-right"><i class="glyphicon glyphicon-minus"></i></div>
	</div>
	<div class="box-container">
		<?php if(action=="index"){ ?>
			<?=$dependencies['add']?>
			<table id="datatable" class="table table-striped table-bordered table-hover dataTable" width="100%">
				<thead><th><?=id?></th><th><?=type_article_name?><th><?="Partida"?></th></th><th><?=actions?></th></thead>
				<tfoot><th><?=id?></th><th><?=type_article_name?><th><?="Partida"?></th></th><th><?=actions?></th></tfoot>
			</table>
            <script>
	            $(document).ready( function () {
	                $('#datatable').dataTable(
		                {
		                	"language":{
		                    	"url": "<?=url_base?>third_party/datatables/language/es.json"
		                        },
	                        "processing": true,
	                        "serverSide": true,
	                        "ordering": false,
	                        "ajax": { url : "<?=url_base.routerCtrl?>/listt", type : "POST" },
	                        "columns": [
	                            { "data": "idtype_article" },
	                            { "data": "name" },
								{ "data": "batch" },
	                            { "data": "btn" }
	                        ]
	                    }
	                ); 
	            });
	        </script>
		<?php }else{ ?>
			<?=(action!="query")? "<form action='".url_base.routerCtrl."/".action."/".$d["idtype_article"]."' method='POST' class='form-horizontal'>" : "<div class='form-horizontal'>" ?>
				<input type="hidden" name="event" id="event">
				<?php
					if(action!="add")
						echo "<div class='form-group'>
							<label class='col-md-2 text-right'>".id.":</label>
							<div class='col-md-3'>
								<input type='text' name='idtype_article' id='idtype_article' value='".$d["idtype_article"]."' class='width-full' disabled data-toggle='tooltip' title='".id_title."'>
							</div>
						</div>";
				?>
				<div class="form-group">
					<label class="col-md-2 text-right"><?=type_article_name?>:</label>
					<div class="col-md-3">
						<input type="text" name="name" id="name" value="<?=$d["name"]?>" aajs="required,blur{exist();}" class="width-full" <?=(action=="query")?'disabled':''?> data-toggle="tooltip" title="<?=type_article_name_title?>" placeholder="<?=type_article_name_placeholder?>">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 text-right"><?=type_article_code?>:</label>
					<div class="col-md-3">
						<input type="text" name="code" id="code" value="<?=$d["code"]?>" aajs="required,number" class="width-full" <?=(action=="query")?'disabled':''?> data-toggle="tooltip" title="<?=type_article_code_title?>" placeholder="<?=type_article_code_placeholder?>">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 text-right">Partida:</label>
					<div class="col-md-3">
						<input type="text" id="batch" value="<?=$d["idbatch"]?>" aajs="searchajax{<?=url_base?>batch/search,this},required" class="width-full" data-toggle="tooltip" title="<?=article_idbatch_title?>" placeholder="<?=article_idbatch_placeholder?>" autocomplete="off">
						<input type="hidden" name="idbatch" id="idbatch" value="<?=$d["idbatch"]?>" aajs="required">
					</div>
				</div>
				<?php
					if(action!="query")
						echo"<div class='form-group'>
							<div class='col-md-2 col-md-offset-5'>
								<button class='btn1' aajs='send'>".save."</button>
							</div>
						</div>";
				?>
			<?=(action!="query")? "</form>" :'</div>' ?>
		<?php } ?>
	</div>
</div>
<script>
	function exist(){
		var field = document.getElementById("name");
		$.post("<?=url_base?>type_article/exist",{value:field.value},function(data){
			var d = $.parseJSON(data);
			if(d[0]["namex"] == field.value){
				toastr.error('Registro existente','',{progressBar:true})
				field.value="";
			}
		});
	}
</script>