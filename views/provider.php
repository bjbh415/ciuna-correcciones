<?php $_SESSION["title"] = provider ?>
<div class="box">
	<div class="box-tools">
		<div class="box-tool-left">
			<a href="<?=url_base?>home/dashboard"><?=dashboard?></a> <i class="fa fa-angle-right"></i> <a href="<?=url_base.routerCtrl?>"><?=provider?></a> <?=(action!="index")? "<i class='fa fa-angle-right'></i> ".((action=="add")? add : ((action=="edit")? edit : query ) ) : ''?>
		</div>
		<div class="box-tool-right"><i class="glyphicon glyphicon-minus"></i></div>
	</div>
	<div class="box-container">
		<?php if(action=="index"){ ?>
			<?=$dependencies['add']?>
			<table id="datatable" class="table table-striped table-bordered table-hover dataTable" width="100%">
                <thead><th><?=id?></th><th><?=provider_identification_card?></th><th><?=provider_name?></th><th><?=actions?></th></thead>
                <tfoot><th><?=id?></th><th><?=provider_identification_card?></th><th><?=provider_name?></th><th><?=actions?></th></tfoot>
            </table>
            <script>
	            $(document).ready( function () {
	                $('#datatable').dataTable(
		                {
		                	"language":{
		                    	"url": "<?=url_base?>third_party/datatables/language/es.json"
		                        },
	                        "processing": true,
	                        "serverSide": true,
	                        "ordering": false,
	                        "ajax": { url : "<?=url_base.routerCtrl?>/listt", type : "POST" },
	                        "columns": [
	                            { "data": "idprovider" },
	                            { "data": "identification_card" },
	                            { "data": "name" },	                            
	                            { "data": "btn" }
	                        ]
	                    }
	                );
	                
	            });
	        </script>
		<?php }else{ ?>
			<?=(action!="query")? "<form name='hola' action='".url_base.routerCtrl."/".action."/".$d["idprovider"]."' method='POST' class='form-horizontal' enctype='multipart/form-data'>" : "<div class='form-horizontal'>" ?>
				<input type="hidden" name="event" id="event">
				<div class="form-group">
					<?php
						if(action!="add")
							echo "<label class='col-md-2 text-right'>".id.":</label>
								<div class='col-md-3'>
									<input type='text' name='idprovider' id='idprovider' value='".$d["idprovider"]."' class='width-full' disabled title='".id_title."'>
								</div>";
					?>
				</div>
				<div class="form-group">
					<label class="col-md-2 text-right"><?=provider_identification_card?>:</label>
					<div class="col-md-2">
						<select name="rif" id="rif" class="width-full" aajs="required" data-toggle="tooltip">
							<option value="V" <?=(substr($d["identification_card"],0,1)=="V")?'selected':''?> >V</option>
							<option value="E" <?=(substr($d["identification_card"],0,1)=="E")?'selected':''?> >E</option>
							<option value="G" <?=(substr($d["identification_card"],0,1)=="G")?'selected':''?> >G</option>
							<option value="J" <?=(substr($d["identification_card"],0,1)=="J")?'selected':''?> >J</option>
							<option value="C" <?=(substr($d["identification_card"],0,1)=="C")?'selected':''?> >C</option>
						</select>
					</div>
					<div class="col-md-4">
						<input type="text" name="identification_card" id="identification_card" value="<?=substr($d["identification_card"],1)?>" aajs="required,number,min{7}" class="width-full" data-toggle="tooltip" title="<?=provider_identification_card_title?>" placeholder="<?=provider_identification_card_placeholder?>">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 text-right"><?=provider_name?>:</label>
					<div class="col-md-4">
						<input type="text" name="name" id="name" value="<?=$d["name"]?>" aajs="required,letter" class="width-full" data-toggle="tooltip" title="<?=provider_name_title?>" placeholder="<?=provider_name_placeholder?>">
					</div>
					<label class="col-md-2 text-right"><?=provider_email?>:</label>
					<div class="col-md-4">
						<input type="text" name="email" id="email" value="<?=$d["email"]?>" aajs="required,email" class="width-full" data-toggle="tooltip" title="<?=provider_email_title?>" placeholder="<?=provider_email_placeholder?>">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 text-right"><?=provider_idaddress?>:</label>
					<div class="col-md-10">
						<input type="text" id="address" value="<?=$d["full_address"]?>" aajs="searchajax{<?=url_base?>parish/search,this},required" class="width-full" data-toggle="tooltip" title="<?=provider_idaddress_title?>" placeholder="<?=provider_idaddress_placeholder?>" autocomplete="off">
						<input type="hidden" name="idaddress" id="idaddress" value="<?=$d["idaddress"]?>" aajs="required">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 text-right"><?=provider_address?>:</label>
					<div class="col-md-10">
						<textarea name="address" id="address" class="width-full" style="height: 80px;resize: none;" data-toggle="tooltip" title="<?=provider_address_title?>" placeholder="<?=provider_address_placeholder?>"><?=$d["address"]?></textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 text-right"><?=provider_phone_one?>:</label>
					<div class="col-md-4">
						<input type="text" name="phone_one" id="phone_one" value="<?=$d["phone_one"]?>" aajs="number" class="width-full" data-toggle="tooltip" title="<?=provider_phone_one_title?>" placeholder="<?=provider_phone_one_placeholder?>" maxlength="11">
					</div>
					<label class="col-md-2 text-right"><?=provider_phone_two?>:</label>
					<div class="col-md-4">
						<input type="text" name="phone_two" id="phone_two" value="<?=$d["phone_two"]?>" aajs="number" class="width-full" data-toggle="tooltip" title="<?=provider_phone_two_title?>" placeholder="<?=provider_phone_two_placeholder?>" maxlength="11">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 text-right"><?=provider_observation?>:</label>
					<div class="col-md-10">
						<textarea name="observation" id="observation" class="width-full" style="height: 80px;resize: none;" data-toggle="tooltip" title="<?=provider_observation_title?>" placeholder="<?=provider_observation_placeholder?>"><?=$d["observation"]?></textarea>
					</div>
				</div>
				<?php
					if(action!="query")
						echo"<div class='form-group'>
							<div class='col-md-2 col-md-offset-5'>
								<button class='btn1' aajs='send'>".save."</button>
							</div>
						</div>";
				?>
			<?=(action!="query")? "</form>" :'</div>' ?>
		<?php } ?>
	</div>
</div>