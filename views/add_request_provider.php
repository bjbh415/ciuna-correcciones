<?php $_SESSION["title"] = movement ?>
<div class="box">
	<div class="box-tools">
		<div class="box-tool-left">
			<a href="<?=url_base?>home/dashboard"><?=dashboard?></a> <i class="fa fa-angle-right"></i> <a href="<?=url_base.routerCtrl?>">SOLICITUDES</a> <?=(action!="index")? "<i class='fa fa-angle-right'></i> ".((action=="add")? add : ((action=="edit")? edit : query ) ) : ''?>
		</div>
		<div class="box-tool-right"><i class="glyphicon glyphicon-minus"></i></div>
	</div>
	<div class="box-container">
		<?php if(action=="index"){ ?>
			<?=$dependencies['add']?>
			<table id="datatable" class="table table-striped table-bordered table-hover dataTable" width="100%">
                <thead><th><?=id?></th><th>PERSONA / DEPARTAMENTO</th><th>Fecha</th><th><?=actions?></th></thead>
                <tfoot><th><?=id?></th><th>PERSONA / DEPARTAMENTO</th><th>Fecha</th><th><?=actions?></th></tfoot>
            </table>
            <script>
	            $(document).ready( function () {
	                $('#datatable').dataTable(
		                {
		                	"language":{
		                    	"url": "<?=url_base?>third_party/datatables/language/es.json"
		                        },
	                        "processing": true,
	                        "serverSide": true,
	                        "ordering": false,
	                        "ajax": { url : "<?=url_base.routerCtrl?>_listt", type : "POST" },
	                        "columns": [
	                            { "data": "idrequest" },
	                            { "data": "name" },
								{ "data": "date_created" },
	                            { "data": "btn" }
	                        ]
	                    }
	                ); 
	            });
	        </script>
		<?php }else{ ?>
			<?=(action!="query")? "<form action='".url_base.routerCtrl."/".action."/".$d["idrequest"]."' method='POST' class='form-horizontal'>" : "<div class='form-horizontal'>" ?>
				<input type="hidden" name="event" id="event">
				<input type="hidden" name="type_movement" id="type_movement" value='<?=(routerCtrl=='entry'? 1 : 2)?>'>
				<?php
					if(action!="add")
						echo "<div class='form-group'>
							<label class='col-md-2 text-right'>".id.":</label>
							<div class='col-md-3'>
								<input type='text' name='idrequest' id='idrequest' value='".$d["idrequest"]."' class='width-full' disabled data-toggle='tooltip' title='".id_title."'>
							</div>
						</div>";
				?>
					<div class="form-group">
						<label class="col-md-4 text-right">SOLICITUD A PROVEEDOR</label>
						<div class="col-md-4">
							
						</div>
					</div>



					<div class="form-group">
<!--FECHA DE ENTRADA-->
						<label class="col-md-2 text-right">Fecha/Hora de Entrada:</label>
						<div class="col-md-4">
							<p><?= date("Y-m-d H:i:s") ?></p>
							<input type="hidden" name="date_created" id="date_created" value="<?= date("Y-m-d H:i:s") ?>">
						</div>
<!--//FECHA DE ENTRADA-->
					</div>
					<div class="form-group">
						<label class="col-md-2 text-right">Observacion:</label>
						<div class="col-md-8">
							<textarea name="observation" id="observation" cols="30" rows="2" class="width-full"><?=$d["observation"]?></textarea>
						</div>
					</div>


				<div class="row">
					<div class="col-md-10 col-md-offset-1">
						<table id="datatable" class="table table-striped table-bordered table-hover dataTable" width="100%">
							<thead>
								<th><?=movement_article?></th>
								<th>Unidad de medida</th>
								<th>Tipo de Artículo</th>
								<th><?=movement_amount?></th>
							</thead>
							
							<tbody id="bodyTable">
								
								<?php
								if(action!='add'){
									foreach($dependencies["movement_articles"] as $key =>  $ma){
										echo"<tr>
											<td>".$ma["name"]."<input type='hidden' name='idarticle[]' id='idarticle-".$key."' value='".$ma["idarticle"]."' data-text='".$ma["name"]."'></td>											
											<td>".$ma["measurement"]."<input type='hidden' name='measurement[]' value='".$ma["measurement"]."'></td>
											<td>".$ma["type_article"]."<input type='hidden' name='type_article[]' id='type_article' value='".$ma["type_article"]."'></td>
											<td>".$ma["amount"]."<input type='hidden' name='amount[]' value='".$ma["amount"]."'></td>
										</tr>";
									}
								}else{
									foreach($dependencies["articles"] as $key =>  $article){
										echo"<tr>
											<td>".$article["name"]."<input type='hidden' name='idarticle[]' id='idarticle-".$key."' value='".$article["idarticle"]."' data-text='".$article["name"]."'></td>											
											<td>".$article["measurement"]."<input type='hidden' name='measurement[]' value='".$article["measurement"]."'></td>
											<td>".$article["type_article"]."<input type='hidden' name='type_article[]' id='type_article' value='".$article["type_article"]."'></td>
											<td><input type='text' name='amount[]' class='amount' data-maxamount='".$article["reqprovamount"]."' value='".$article["reqprovamount"]."'></td>
										</tr>";
									}
								}
									
								?>
							</tbody>
						</table>
					</div>
				</div>
				<?php
					if(action!="query")
						echo"<div class='form-group'>
							<div class='col-md-2 col-md-offset-5'>
								<button class='btn1' aajs='send'>".save."</button>
							</div>
						</div>";
				?>
			<?=(action!="query")? "</form>" :'</div>' ?>
		<?php } ?>
	</div>
</div>
<script>
var cont = 1000;
$("#amount").blur(function(){
	var amount = Number(this.value);
	var totalAmount = Number($("#article option:selected").data("amount"));
	var x = {};
	if(amount > totalAmount)x = {ok:false, message: 'La cantidad de entrada no puede ser mayor a: ' + totalAmount};
	else if(amount < 0) x = {ok:false, message: 'La cantidad no puede ser menor a cero'};
	else if(isNaN(amount)) x = {ok:false, message: 'La cantidad debe ser numerica'};
	else x = {ok:true};

	if(!x.ok) {
		this.value = null;
		toastr.error('Error: ' + x.message,'',{progressBar:true});
		return false;
	}
	
});
$("#add").click(function(){
	var measurement = $("#measurement").text();
	var type_article = $("#type_article").text();
	var articleVal = $("#article").val();
	var articleText = $("#article option[value='"+articleVal+"']").text();
	var amount = $("#amount").val();
	if(articleVal=="0"){
		toastr.error('<?=movement_article_add_error?>','',{progressBar:true});
		return false;
	}
	if(amount==""){
		toastr.error('<?=movement_amount_add_error?>','',{progressBar:true});
		return false;
	}
	/*if(code==""){
		code="<?=movement_code_no?>";
	}*/
	$("#article option[value='"+$("#article").val()+"']").remove();
	var html = `<tr>
					<td>`+articleText+`<input type="hidden" name="idarticle[]" id="idarticle-`+cont+`" value="`+articleVal+`" data-text="`+articleText+`"></td>
					<td>`+measurement+`</td>
					<td>`+type_article+`</td>
					<td>`+amount+`<input type="hidden" name="amount[]" value="`+amount+`"></td>
					<td><button type="button" id="`+cont+`" class="btn1" onclick="remove(this);"><i class="fa fa-minus"></i></button></td>
				</tr>`;
	$("#bodyTable").append(html);
	cont++;
});
function remove(_this){
	var val = $("#idarticle-"+_this.id).val();
	var text = $("#idarticle-"+_this.id).attr("data-text");
	console.log(val+" "+text);
	$("#article").append("<option value='"+val+"'>"+text+"</option>");
	$(_this).parent().parent().remove();
}
function search(){
	$.post("<?=url_base?>article/search",{value:$("#article").val()},function(datas){
		var data = $.parseJSON(datas);
		console.log(data);
		if(data!=null){
			$("#measurement").text(data[0][0]);
			$("#type_article").text(data[0][1]);
		}else{
			$("#measurement").text(null);
			$("#type_article").text(null);
		}

	});
}
$(".amount").blur(function(){
	
		var a = $(this).val();
		if(a==null) {
			alert('a es null');
			return false
		};
		var b = $(this).data('maxamount');
		console.log(a, b);
		var x = {};
		if(Number(a) > Number(b)) x = {ok:false, message: 'No puede solicitar más de '+b+' unidades de este artículo'};
		else if(a < 0) x = {ok:false, message: 'La cantidad no puede ser menor a cero'};
		else if(isNaN(a)) x = {ok:false, message: 'La cantidad debe ser numerica'};
		else x = {ok:true};

		if(!x.ok) {
			$(this).val(null);
			toastr.error('Error: ' + x.message,'',{progressBar:true});
			return false;
		}

});

</script>