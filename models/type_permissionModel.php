<?php
namespace Models;
class type_permissionModel{
	private $db,$permission;
	public $idtype_permission,$name,$status;
	public function __construct(){
		$this->db = new \core\ameliaBD;
		$this->permission = new \models\permissionModel;
	}
	public function listt($draw,$search,$start,$length){
		$start = (empty($start))? 0 : $start;
		$length = (empty($length))? 10 : $length;
		$this->db->prepare("SELECT *,(SELECT count(*) FROM ".PREFIX."ttype_permission) as countx FROM ".PREFIX."ttype_permission WHERE CAST(idtype_permission as CHAR) LIKE '%$search%' OR name LIKE '%$search%' ORDER BY idtype_permission DESC LIMIT $length OFFSET $start ");
		$d["data"]= [];$d["recordsFiltered"] = 0;$d["recordsTotal"] = 0;
		foreach ($this->db->execute() as $key => $val) {
			$d["data"][$key]["idtype_permission"] = $val["idtype_permission"];
			$d["data"][$key]["name"] = $val["name"];
			$d["data"][$key]["btn"] = $this->permission->getpermission($val["idtype_permission"],$val["status"]);
			$d["recordsFiltered"] = $val["countx"];
			$d["recordsTotal"]++;
		}
		$d["draw"] = $draw;
		return $d;
	}
	public function dependencies(){
		return $this->permission->getpermissionadd();
	}
	public function add(){
		$this->db->prepare("INSERT INTO ".PREFIX."ttype_permission (name,status) VALUES (?,'1');");
		return $this->db->execute(array($this->name));
	}
	public function query(){
		$this->db->prepare("SELECT * FROM ".PREFIX."ttype_permission WHERE idtype_permission=? ;");
		$data=$this->db->execute(array($this->idtype_permission));
		foreach ($data as $val) { $d=$val; }
		return $d;
	}
	public function edit(){
		$this->db->prepare("UPDATE ".PREFIX."ttype_permission SET name=? WHERE idtype_permission=?;");
		return $this->db->execute(array($this->name,$this->idtype_permission));
	}
	public function delete(){
		$this->db->prepare("DELETE FROM ".PREFIX."ttype_permission WHERE idtype_permission=?;");
		return $this->db->execute(array($this->idtype_permission));
	}
	public function status($num){
		$this->db->prepare("UPDATE ".PREFIX."ttype_permission SET status=? WHERE idtype_permission=?;");
		return $this->db->execute(array($num,$this->idtype_permission));
	}
	public function pdf(){
		$this->db->prepare("SELECT * FROM ".PREFIX."ttype_permission ORDER BY 1 DESC;");
		$data=$this->db->execute();
		foreach ($data as $val) { $d[]=$val; }
		return $d;
	}
}
?>