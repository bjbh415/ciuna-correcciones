<?php
namespace Models;
class measurementModel{
	private $db,$permission;
	public $idmeasurement,$name,$status;
	public function __construct(){
		$this->db = new \core\ameliaBD;
		$this->permission = new \models\permissionModel;
	}
	public function listt($draw,$search,$start,$length){
		$start = (empty($start))? 0 : $start;
		$length = (empty($length))? 10 : $length;
		$this->db->prepare("SELECT *,(SELECT count(*) FROM ".PREFIX."tmeasurement) as countx FROM ".PREFIX."tmeasurement WHERE CAST(idmeasurement as CHAR) LIKE '%$search%' OR name LIKE '%$search%' ORDER BY idmeasurement DESC LIMIT $length OFFSET $start ");
		$d["data"]= [];$d["recordsFiltered"] = 0;$d["recordsTotal"] = 0;
		foreach ($this->db->execute() as $key => $val) {
			$d["data"][$key]["idmeasurement"] = $val["idmeasurement"];
			$d["data"][$key]["name"] = $val["name"];
			$d["data"][$key]["btn"] = $this->permission->getpermission($val["idmeasurement"],$val["status"]);
			$d["recordsFiltered"] = $val["countx"];
			$d["recordsTotal"]++;
		}
		$d["draw"] = $draw;
		return $d;
	}
	public function dependencies(){
		return $this->permission->getpermissionadd();
	}
	public function add(){
		$this->db->prepare("INSERT INTO ".PREFIX."tmeasurement (name,status) VALUES (?,'1');");
		return $this->db->execute(array($this->name));
	}
	public function query(){
		$this->db->prepare("SELECT * FROM ".PREFIX."tmeasurement WHERE idmeasurement=? ;");
		$data=$this->db->execute(array($this->idmeasurement));
		foreach ($data as $val) { $d=$val; }
		return $d;
	}
	public function edit(){
		$this->db->prepare("UPDATE ".PREFIX."tmeasurement SET name=? WHERE idmeasurement=?;");
		return $this->db->execute(array($this->name,$this->idmeasurement));
	}
	public function delete(){
		$this->db->prepare("DELETE FROM ".PREFIX."tmeasurement WHERE idmeasurement=?;");
		return $this->db->execute(array($this->idmeasurement));
	}
	public function status($num){
		$this->db->prepare("UPDATE ".PREFIX."tmeasurement SET status=? WHERE idmeasurement=?;");
		return $this->db->execute(array($num,$this->idmeasurement));
	}
	public function pdf(){
		$this->db->prepare("SELECT * FROM ".PREFIX."tmeasurement ORDER BY 1 DESC;");
		$data=$this->db->execute();
		foreach ($data as $val) { $d[]=$val; }
		return $d;
	}
	public function exist($value){
		$this->db->prepare("SELECT lower(name) as namex FROM ".PREFIX."tmeasurement WHERE lower(name) = '$value' AND status='1';");
		$data=$this->db->execute();
		foreach ($data as $val) { $d[]=$val; }
		return $d;
	}
}
?>