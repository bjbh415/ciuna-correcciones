<?php
namespace Models;
class batchModel{
	private $db,$permission;
	public $idbatch,$code,$name,$status;
	public function __construct(){
		$this->db = new \core\ameliaBD;
		$this->permission = new \models\permissionModel;
	}
	public function listt($draw,$search,$start,$length){
		$start = (empty($start))? 0 : $start;
		$length = (empty($length))? 10 : $length;
		$this->db->prepare("SELECT *,(SELECT count(*) FROM ".PREFIX."tbatch) as countx FROM ".PREFIX."tbatch WHERE CAST(idbatch as CHAR) LIKE '%$search%' OR name LIKE '%$search%' ORDER BY idbatch DESC LIMIT $length OFFSET $start ");
		$d["data"]= [];$d["recordsFiltered"] = 0;$d["recordsTotal"] = 0;
		foreach ($this->db->execute() as $key => $val) {
			$d["data"][$key]["idbatch"] = $val["idbatch"];
			$d["data"][$key]["name"] = $val["name"];
			$d["data"][$key]["btn"] = $this->permission->getpermission($val["idbatch"],$val["status"]);
			$d["recordsFiltered"] = $val["countx"];
			$d["recordsTotal"]++;
		}
		$d["draw"] = $draw;
		return $d;
	}
	public function dependencies(){
		return $this->permission->getpermissionadd();
	}
	public function add(){
		$this->db->prepare("INSERT INTO ".PREFIX."tbatch (name,code,status) VALUES (?,?,'1');");
		return $this->db->execute(array($this->name,$this->code));
	}
	public function query(){
		$this->db->prepare("SELECT * FROM ".PREFIX."tbatch WHERE idbatch=? ;");
		$data=$this->db->execute(array($this->idbatch));
		foreach ($data as $val) { $d=$val; }
		return $d;
	}
	public function edit(){
		$this->db->prepare("UPDATE ".PREFIX."tbatch SET name=?,code=? WHERE idbatch=?;");
		return $this->db->execute(array($this->name,$this->code,$this->idbatch));
	}
	public function delete(){
		$this->db->prepare("DELETE FROM ".PREFIX."tbatch WHERE idbatch=?;");
		return $this->db->execute(array($this->idbatch));
	}
	public function status($num){
		$this->db->prepare("UPDATE ".PREFIX."tbatch SET status=? WHERE idbatch=?;");
		return $this->db->execute(array($num,$this->idbatch));
	}
	public function exist($value){
		$this->db->prepare("SELECT lower(name) as namex FROM ".PREFIX."tbatch WHERE lower(name) = '$value' AND status='1';");
		$data=$this->db->execute();
		foreach ($data as $val) { $d[]=$val; }
		return $d;
	}
	public function search($value){
		$this->db->prepare("SELECT idbatch,CONCAT(name,' - ',code) as name FROM ".PREFIX."tbatch WHERE lower(name) LIKE lower('%$value%') AND status='1';");
		$data=$this->db->execute();
		foreach ($data as $val) { $d[]=$val; }
		return $d;
	}
	public function pdf(){
		$this->db->prepare("SELECT * FROM ".PREFIX."tbatch ORDER BY 1 DESC;");
		$data=$this->db->execute();
		foreach ($data as $val) { $d[]=$val; }
		return $d;
	}
}
?>