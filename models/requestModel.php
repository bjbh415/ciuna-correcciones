<?php
namespace Models;
class requestModel{
	private $db,$permission;
	public $idrequest,$type_request,$idtype_entry,$idprovider,$iddepartament,$iduser,$status,$date_register,$idrequest_article,$code,$idarticle,$amount;
	public function __construct(){
		$this->db = new \core\ameliaBD;
		$this->permission = new \models\permissionModel;
	}
	public function listt($draw,$search,$start,$length,$type){
		$start = (empty($start))? 0 : $start;
		$length = (empty($length))? 10 : $length;
		if($type==1){
			$this->db->prepare("SELECT r.idrequest,CONCAT(p.name_one,' ',p.last_name_one,' / ',d.name) as name,DATE_FORMAT(r.date_created, '%d-%m-%Y') as date_created,(SELECT count(*) FROM ".PREFIX."trequest) as countx, rc.code AS code 
			FROM ".PREFIX."trequest_code rc 
			INNER JOIN ".PREFIX."trequest r ON rc.idrequest = r.idrequest
			INNER JOIN ".PREFIX."tuser u ON r.iduser=u.iduser
			INNER JOIN ".PREFIX."tperson p ON u.idperson=p.idperson
			INNER JOIN ".PREFIX."tdepartament d ON p.idperson=d.idperson
			WHERE ((CAST(r.idrequest as CHAR) LIKE '%$search%' 
			OR CAST(r.iduser as CHAR) LIKE '%$search%' ))".(($_SESSION['idcharge']===1)?"":" AND p.idperson = $_SESSION[idcharge] ")." 
			ORDER BY r.idrequest DESC LIMIT $length OFFSET $start ");
		}else{
			$this->db->prepare("SELECT *,(SELECT count(*) FROM ".PREFIX."trequest) as countx FROM ".PREFIX."trequest r WHERE (CAST(m.idmovement as CHAR) LIKE '%$search%' OR p.identification_card LIKE '%$search%' ORDER BY m.idmovement DESC LIMIT $length OFFSET $start ");
		}
		$d["data"]= [];$d["recordsFiltered"] = 0;$d["recordsTotal"] = 0;
		foreach ($this->db->execute() as $key => $val) {
			$d["data"][$key]["idrequest"] = $val["idrequest"];
			$d["data"][$key]["code"] = $val["code"];
			$d["data"][$key]["name"] = $val["name"];
			$d["data"][$key]["date_created"] = $val["date_created"];
			$d["data"][$key]["btn"] = $this->permission->getpermission($val["idrequest"],$val["status"]);
			$d["recordsFiltered"] = $val["countx"];
			$d["recordsTotal"]++;
		}
		$d["draw"] = $draw;
		return $d;
	}
	public function dependencies(){
		$this->db->prepare("SELECT idtype_entry,name FROM ".PREFIX."ttype_entry WHERE status='1';");
		$dependencies["type_entrys"] = $this->db->execute();
		$this->db->prepare("SELECT iddepartament,name FROM ".PREFIX."tdepartament WHERE status='1';");
		$dependencies["departaments"] = $this->db->execute();
		$this->db->prepare("SELECT a.idarticle,CONCAT(a.name,'-',m.name,'-',b.name) as name,a.amount FROM ".PREFIX."tarticle a INNER JOIN ".PREFIX."tmodel m ON a.idmodel=m.idmodel INNER JOIN ".PREFIX."tbrand b ON m.idbrand=b.idbrand INNER JOIN ".PREFIX."ttype_article ta ON a.idtype_article=ta.idtype_article WHERE a.status='1';");
		$dependencies["articles"] = $this->db->execute();
		$this->db->prepare("SELECT a.idarticle,CONCAT(a.name,'-',m.name,'-',b.name,' | En existencia: ',a.amount) as name,ma.amount FROM ".PREFIX."tdrequest ma INNER JOIN ".PREFIX."tarticle a ON ma.idarticle=a.idarticle INNER JOIN ".PREFIX."tmodel m ON a.idmodel=m.idmodel INNER JOIN ".PREFIX."tbrand b ON m.idbrand=b.idbrand INNER JOIN ".PREFIX."ttype_article ta ON a.idtype_article=ta.idtype_article WHERE ma.idrequest=?;");
		$this->db->execute(array($this->idrequest));
		$dependencies["movement_articles"] = $this->db->fetchAll();
		$this->db->prepare("SELECT a.idarticle,CONCAT(a.name,'-',m.name,'-',b.name,' | En existencia: ',a.amount) as name,a.amount FROM ".PREFIX."tarticle a INNER JOIN ".PREFIX."tmodel m ON a.idmodel=m.idmodel INNER JOIN ".PREFIX."tbrand b ON m.idbrand=b.idbrand INNER JOIN ".PREFIX."ttype_article ta ON a.idtype_article=ta.idtype_article WHERE a.idarticle NOT IN (SELECT idarticle FROM ".PREFIX."tdrequest WHERE idrequest=? );");
		$this->db->execute(array($this->idrequest));
		$dependencies["articles_edit"] = $this->db->fetchAll();
		$dependencies["add"] = $this->permission->getpermissionadd();
		return $dependencies;
	}
	public function add(){
		$this->db->prepare("INSERT INTO ".PREFIX."trequest (iduser,date_created,status) VALUES (?,NOW(),'1')");
		return $this->db->execute(array($_SESSION["iduser"]));
	}
	public function add_two($add){
		$this->db->prepare("SELECT idrequest FROM ".PREFIX."trequest ORDER BY idrequest DESC LIMIT 1 OFFSET 0;");
		$this->db->execute();
		$d=$this->db->fetchAll();
		$this->db->prepare("INSERT INTO ".PREFIX."tdrequest (idrequest,idarticle,amount) VALUES (?,?,?)");
		return $this->db->execute(array(($add==1? $d[0]["idrequest"] : $this->idrequest),$this->idarticle,$this->amount));
	}
	public function query($type){
		if($type==1){
			$this->db->prepare("SELECT r.idrequest,CONCAT(p.name_one,' ',p.last_name_one,' / ',d.name) as name,DATE_FORMAT(r.date_created, '%d-%m-%Y') as date_created,(SELECT count(*) FROM ".PREFIX."trequest) as countx, rc.code AS code 
			FROM ".PREFIX."trequest_code rc 
			INNER JOIN ".PREFIX."trequest r ON rc.idrequest = r.idrequest
			INNER JOIN ".PREFIX."tuser u ON r.iduser=u.iduser
			INNER JOIN ".PREFIX."tperson p ON u.idperson=p.idperson
			INNER JOIN ".PREFIX."tdepartament d ON p.idperson=d.idperson
			WHERE r.idrequest=?");
		}else{
			$this->db->prepare("SELECT *,d.name as full_provider FROM ".PREFIX."trequest m INNER JOIN ".PREFIX."tdepartament d ON m.iddepartament=d.iddepartament WHERE idrequest=? ;");
		}
		$data=$this->db->execute(array($this->idrequest));
		foreach ($data as $val) { $d=$val; }
		return $d;
	}
	public function edit(){
		//$this->db->prepare("UPDATE ".PREFIX."trequest SET type_request=?,idtype_entry=?,idprovider=?,iddepartament=? WHERE idrequest=?;");
		//return $this->db->execute(array($this->type_request,$this->idtype_entry,$this->idprovider,$this->iddepartament,$this->idrequest));
		return true;
	}
	public function delete_ma(){
		$this->db->prepare("DELETE FROM ".PREFIX."tdrequest WHERE idrequest=?;");
		return $this->db->execute(array($this->idrequest));
	}
	public function delete(){
		$this->db->prepare("DELETE FROM ".PREFIX."trequest WHERE idrequest=?;");
		return $this->db->execute(array($this->idrequest));
	}
	public function status($num){
		$this->db->prepare("UPDATE ".PREFIX."trequest SET status=? WHERE idrequest=?;");
		return $this->db->execute(array($num,$this->idrequest));
	}
	public function pdf(){		
		$this->db->prepare("SELECT r.idrequest,CONCAT(p.name_one,' ',p.last_name_one) as responsable, d.name as departament, DATE_FORMAT(r.date_created, '%d-%m-%Y') as date_created,(SELECT count(*) FROM ".PREFIX."trequest) as countx, rc.code AS code 
		FROM ".PREFIX."trequest_code rc 
		INNER JOIN ".PREFIX."trequest r ON rc.idrequest = r.idrequest
		INNER JOIN ".PREFIX."tuser u ON r.iduser=u.iduser
		INNER JOIN ".PREFIX."tperson p ON u.idperson=p.idperson
		INNER JOIN ".PREFIX."tdepartament d ON p.idperson=d.idperson");

		foreach ($this->db->execute() as $key => $val) {
			$d[$key]["idrequest"] = $val["idrequest"];
			$d[$key]["code"] = $val["code"];
			$d[$key]["responsable"] = $val["responsable"];
			$d[$key]["departament"] = $val["departament"];
			$d[$key]["date_created"] = $val["date_created"];
			$d[$key]['articles'] = [];

			$this->db->prepare("SELECT a.idarticle,CONCAT(a.name,'-',m.name,'-',b.name,' (',r.amount,')') as article, r.amount 
			FROM ".PREFIX."tdrequest r
			INNER JOIN ".PREFIX."tarticle a ON r.idarticle=a.idarticle 
			INNER JOIN ".PREFIX."tmodel m ON a.idmodel=m.idmodel 
			INNER JOIN ".PREFIX."tbrand b ON m.idbrand=b.idbrand 
			INNER JOIN ".PREFIX."ttype_article ta ON a.idtype_article=ta.idtype_article 
			INNER JOIN acms_tmeasurement ms ON ms.idmeasurement = a.idmeasurement 
			WHERE r.idrequest=?;");

			foreach ($this->db->execute(array($d[$key]["idrequest"])) as $key2 => $val2){
				$d[$key]['articles'][] =  $val2['article'];
			}
			$d[$key]['articles'] = implode("\n", $d[$key]['articles']);
		}
		return $d;
	}
	public function search($value){

		$this->db->prepare("SELECT r.idrequest, r.date_created, d.iddepartament, rc.code, CONCAT(p.name_one,' ',p.last_name_one,' / ',d.name) as applicant
		FROM ".PREFIX."trequest_code rc 
		INNER JOIN ".PREFIX."trequest r ON rc.idrequest=r.idrequest 
		INNER JOIN ".PREFIX."tuser u ON r.iduser=u.iduser
		INNER JOIN ".PREFIX."tperson p ON u.idperson=p.idperson
		INNER JOIN ".PREFIX."tdepartament d ON p.idperson=d.idperson
		WHERE rc.code=?");
		$data=$this->db->execute(array($value));
		foreach ($data as $val) { $d[]=$val; }
		
		$this->db->prepare("SELECT a.idarticle,CONCAT(a.name,'-',m.name,'-',b.name,' | En existencia: ',a.amount) as name,ma.amount, a.amount as totalamount FROM ".PREFIX."tdrequest ma INNER JOIN ".PREFIX."tarticle a ON ma.idarticle=a.idarticle INNER JOIN ".PREFIX."tmodel m ON a.idmodel=m.idmodel INNER JOIN ".PREFIX."tbrand b ON m.idbrand=b.idbrand INNER JOIN ".PREFIX."ttype_article ta ON a.idtype_article=ta.idtype_article WHERE ma.idrequest=?;");
		$this->db->execute(array($d[0]['idrequest']));
		$d["requested_articles"] = $this->db->fetchAll();

		return $d;
	}
}
?>