<?php
namespace Models;
class type_articleModel{
	private $db,$permission;
	public $idtype_article,$code,$name,$status, $idbatch;
	public function __construct(){
		$this->db = new \core\ameliaBD;
		$this->permission = new \models\permissionModel;
	}
	public function listt($draw,$search,$start,$length){
		$start = (empty($start))? 0 : $start;
		$length = (empty($length))? 10 : $length;
		$this->db->prepare("SELECT ta.idtype_article, ta.name AS name, b.name AS batch, (SELECT count(*) FROM ".PREFIX."ttype_article) as countx FROM ".PREFIX."ttype_article ta INNER JOIN ".PREFIX."tbatch b ON ta.idbatch = b.idbatch WHERE CAST(ta.idtype_article as CHAR) LIKE '%$search%' OR ta.name LIKE '%$search%' ORDER BY ta.idtype_article DESC LIMIT $length OFFSET $start ");
		$d["data"]= [];$d["recordsFiltered"] = 0;$d["recordsTotal"] = 0;
		foreach ($this->db->execute() as $key => $val) {
			$d["data"][$key]["idtype_article"] = $val["idtype_article"];
			$d["data"][$key]["name"] = $val["name"];
			$d["data"][$key]["batch"] = $val["batch"];
			$d["data"][$key]["btn"] = $this->permission->getpermission($val["idtype_article"],$val["status"]);
			$d["recordsFiltered"] = $val["countx"];
			$d["recordsTotal"]++;
		}
		$d["draw"] = $draw;
		return $d;
	}
	public function dependencies(){
		$this->db->prepare("SELECT * FROM ".PREFIX."tbatch WHERE status='1';");
		$dependencies["batches"] = $this->db->execute(array());
		$dependencies["add"] = $this->permission->getpermissionadd();
		return $dependencies;
	}
	public function add(){
		$this->db->prepare("INSERT INTO ".PREFIX."ttype_article (name,code,status,idbatch) VALUES (?,?,'1',?);");
		return $this->db->execute(array($this->name,$this->code, $this->idbatch));
	}
	public function query(){
		$this->db->prepare("SELECT * FROM ".PREFIX."ttype_article WHERE idtype_article=? ;");
		$data=$this->db->execute(array($this->idtype_article));
		foreach ($data as $val) { $d=$val; }
		return $d;
	}
	public function edit(){
		$this->db->prepare("UPDATE ".PREFIX."ttype_article SET name=?,code=?, idbatch=? WHERE idtype_article=?;");
		return $this->db->execute(array($this->name,$this->code,$this->idbatch,$this->idtype_article));
	}
	public function delete(){
		$this->db->prepare("DELETE FROM ".PREFIX."ttype_article WHERE idtype_article=?;");
		return $this->db->execute(array($this->idtype_article));
	}
	public function status($num){
		$this->db->prepare("UPDATE ".PREFIX."ttype_article SET status=? WHERE idtype_article=?;");
		return $this->db->execute(array($num,$this->idtype_article));
	}
	public function exist($value){
		$this->db->prepare("SELECT lower(name) as namex FROM ".PREFIX."ttype_article WHERE lower(name) = '$value' AND status='1';");
		$data=$this->db->execute();
		foreach ($data as $val) { $d[]=$val; }
		return $d;
	}
	public function search($value){
		$this->db->prepare("SELECT idtype_article,CONCAT(name,' - ',code) as name FROM ".PREFIX."ttype_article WHERE lower(name) LIKE lower('%$value%') AND status='1';");
		$data=$this->db->execute();
		foreach ($data as $val) { $d[]=$val; }
		return $d;
	}
	public function pdf(){
		$this->db->prepare("SELECT * FROM ".PREFIX."ttype_article ORDER BY 1 DESC;");
		$data=$this->db->execute();
		foreach ($data as $val) { $d[]=$val; }
		return $d;
	}
}
?>