<?php
namespace Models;
class articleModel{
	private $db,$permission;
	public $idarticle,$idmodel,$idtype_article,$name,$status,$idmeasurement,$stockmin,$stockmax;
	public function __construct(){
		$this->db = new \core\ameliaBD;
		$this->permission = new \models\permissionModel;
	}
	public function listt($draw,$search,$start,$length){
		$start = (empty($start))? 0 : $start;
		$length = (empty($length))? 10 : $length;
		$this->db->prepare("SELECT a.*,(SELECT count(*) FROM ".PREFIX."tarticle) as countx, ta.name as type_article FROM ".PREFIX."tarticle a INNER JOIN ".PREFIX."ttype_article ta ON a.idtype_article=ta.idtype_article WHERE CAST(a.idarticle as CHAR) LIKE '%$search%' OR a.name LIKE '%$search%' ORDER BY a.idarticle DESC LIMIT $length OFFSET $start ");
		$d["data"]= [];$d["recordsFiltered"] = 0;$d["recordsTotal"] = 0;
		foreach ($this->db->execute() as $key => $val) {
			$d["data"][$key]["idarticle"] = $val["idarticle"];
			$d["data"][$key]["name"] = $val["name"];
			$d["data"][$key]["type_article"] = $val["type_article"];
			$d["data"][$key]["btn"] = $this->permission->getpermission($val["idarticle"],$val["status"]);
			$d["recordsFiltered"] = $val["countx"];
			$d["recordsTotal"]++;
		}
		$d["draw"] = $draw;
		return $d;
	}
	public function dependencies(){
		$this->db->prepare("SELECT * FROM ".PREFIX."tmeasurement WHERE status='1';");
		$dependencies["measurements"] = $this->db->execute(array());
		$dependencies["add"] = $this->permission->getpermissionadd();
		return $dependencies;
	}
	public function add(){
		$this->db->prepare("INSERT INTO ".PREFIX."tarticle (idmodel,idtype_article,name,amount,stockmin,status,idmeasurement, stockmax) VALUES (?,?,?,?,?,'1',?,?);");
		return $this->db->execute(array($this->idmodel,$this->idtype_article,$this->name,0,$this->stockmin,$this->idmeasurement,$this->stockmax));
	}
	public function query(){
		$this->db->prepare("SELECT a.idarticle,m.idmodel,m.name as model,ta.idtype_article,ta.name as type_article,a.name,a.amount,a.stockmin, a.stockmax,a.status,a.idmeasurement FROM ".PREFIX."tarticle a INNER JOIN ".PREFIX."tmodel m ON a.idmodel=m.idmodel INNER JOIN ".PREFIX."ttype_article ta ON a.idtype_article=ta.idtype_article WHERE a.idarticle=? ;");
		$data=$this->db->execute(array($this->idarticle));
		foreach ($data as $val) { $d=$val; }
		return $d;
	}
	public function edit(){
		$this->db->prepare("UPDATE ".PREFIX."tarticle SET idmodel=?,idtype_article=?,name=?,stockmin=?,idmeasurement=?,stockmax=? WHERE idarticle=?;");
		return $this->db->execute(array($this->idmodel,$this->idtype_article,$this->name,$this->stockmin,$this->idmeasurement,$this->stockmax,$this->idarticle));
	}
	public function delete(){
		$this->db->prepare("DELETE FROM ".PREFIX."tarticle WHERE idarticle=?;");
		return $this->db->execute(array($this->idarticle));
	}
	public function status($num){
		$this->db->prepare("UPDATE ".PREFIX."tarticle SET status=? WHERE idarticle=?;");
		return $this->db->execute(array($num,$this->idarticle));
	}
	public function exist($value){
		$this->db->prepare("SELECT lower(name) as namex FROM ".PREFIX."tarticle WHERE lower(name) = lower('$value') AND status='1';");
		$data=$this->db->execute();
		foreach ($data as $val) { $d[]=$val; }
		return $d;
	}
	public function search($value){
		$this->db->prepare("SELECT m.name as measurement,ta.name as type_article, (a.stockmax-a.amount) as reqprovamount FROM ".PREFIX."tarticle a INNER JOIN ".PREFIX."tmeasurement m ON a.idmeasurement=m.idmeasurement INNER JOIN ".PREFIX."ttype_article ta ON a.idtype_article=ta.idtype_article WHERE a.idarticle=? ;");
		$data=$this->db->execute(array($value));
		foreach ($data as $val) { $d[]=$val; }
		return $d;
	}
	public function pdf(){
		$this->db->prepare("SELECT a.*,(SELECT count(*) FROM ".PREFIX."tarticle) as countx, ta.name as type_article FROM ".PREFIX."tarticle a INNER JOIN ".PREFIX."ttype_article ta ON a.idtype_article=ta.idtype_article ORDER BY a.idarticle DESC");
		$d= [];
		foreach ($this->db->execute() as $key => $val) {
			$d[$key]["idarticle"] = $val["idarticle"];
			$d[$key]["name"] = $val["name"];
			$d[$key]["type_article"] = $val["type_article"];
			$d[$key]["name"] = $val["name"];
			$d[$key]["status"] = $val["status"];
		}
		return $d;
	}
}
?>
