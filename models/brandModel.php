<?php
namespace Models;
class brandModel{
	private $db,$permission;
	public $idbrand,$name,$status;
	public function __construct(){
		$this->db = new \core\ameliaBD;
		$this->permission = new \models\permissionModel;
	}
	public function listt($draw,$search,$start,$length){
		$start = (empty($start))? 0 : $start;
		$length = (empty($length))? 10 : $length;
		$this->db->prepare("SELECT *,(SELECT count(*) FROM ".PREFIX."tbrand) as countx FROM ".PREFIX."tbrand WHERE CAST(idbrand as CHAR) LIKE '%$search%' OR name LIKE '%$search%' ORDER BY idbrand DESC LIMIT $length OFFSET $start ");
		$d["data"]= [];$d["recordsFiltered"] = 0;$d["recordsTotal"] = 0;
		foreach ($this->db->execute() as $key => $val) {
			$d["data"][$key]["idbrand"] = $val["idbrand"];
			$d["data"][$key]["name"] = $val["name"];
			$d["data"][$key]["btn"] = $this->permission->getpermission($val["idbrand"],$val["status"]);
			$d["recordsFiltered"] = $val["countx"];
			$d["recordsTotal"]++;
		}
		$d["draw"] = $draw;
		return $d;
	}
	public function dependencies(){
		return $this->permission->getpermissionadd();
	}
	public function add(){
		$this->db->prepare("INSERT INTO ".PREFIX."tbrand (name,status) VALUES (?,'1');");
		return $this->db->execute(array($this->name));
	}
	public function query(){
		$this->db->prepare("SELECT * FROM ".PREFIX."tbrand WHERE idbrand=? ;");
		$data=$this->db->execute(array($this->idbrand));
		foreach ($data as $val) { $d=$val; }
		return $d;
	}
	public function edit(){
		$this->db->prepare("UPDATE ".PREFIX."tbrand SET name=? WHERE idbrand=?;");
		return $this->db->execute(array($this->name,$this->idbrand));
	}
	public function delete(){
		$this->db->prepare("DELETE FROM ".PREFIX."tbrand WHERE idbrand=?;");
		return $this->db->execute(array($this->idbrand));
	}
	public function status($num){
		$this->db->prepare("UPDATE ".PREFIX."tbrand SET status=? WHERE idbrand=?;");
		return $this->db->execute(array($num,$this->idbrand));
	}
	public function search($value){
		$this->db->prepare("SELECT idbrand,name FROM ".PREFIX."tbrand WHERE lower(name) LIKE lower('%$value%') AND status='1';");
		$data=$this->db->execute();
		foreach ($data as $val) { $d[]=$val; }
		return $d;
	}
	public function exist($value){
		$this->db->prepare("SELECT lower(name) as namex FROM ".PREFIX."tbrand WHERE lower(name) = '$value' AND status='1';");
		$data=$this->db->execute();
		foreach ($data as $val) { $d[]=$val; }
		return $d;
	}
	public function pdf(){
		$this->db->prepare("SELECT * FROM ".PREFIX."tbrand ORDER BY 1 DESC;");
		$data=$this->db->execute();
		foreach ($data as $val) { $d[]=$val; }
		return $d;
	}
}
?>