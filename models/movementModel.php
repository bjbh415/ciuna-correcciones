<?php
namespace Models;
class movementModel{
	private $db,$permission;
	public $idmovement,$type_movement,$idtype_entry,$idprovider,$iddepartament,$iduser,$status,$date_register,$idmovement_article,$code,$idarticle,$amount, $number_billing_request, $date_billing_request;
	public function __construct(){
		$this->db = new \core\ameliaBD;
		$this->permission = new \models\permissionModel;
	}
	public function listt($draw,$search,$start,$length,$type){
		$start = (empty($start))? 0 : $start;
		$length = (empty($length))? 10 : $length;
		if($type==1){
			$this->db->prepare("SELECT m.idmovement,CONCAT(p.identification_card,'-',p.name,' / ',DATE_FORMAT(m.date_register,'%d-%m-%Y %H:%i')) as name,m.status,(SELECT count(*) FROM ".PREFIX."tmovement) as countx FROM ".PREFIX."tmovement m INNER JOIN ".PREFIX."tprovider p ON m.idprovider=p.idprovider WHERE (CAST(m.idmovement as CHAR) LIKE '%$search%' OR p.identification_card LIKE '%$search%' OR p.name LIKE '%$search%' OR CAST(m.date_register as CHAR) LIKE '%$search%') AND m.type_movement='1' ORDER BY m.idmovement DESC LIMIT $length OFFSET $start ");
		}else{
			$this->db->prepare("SELECT m.idmovement,CONCAT(d.name,' / ',DATE_FORMAT(m.date_register,'%d-%m-%Y %H:%i')) as name,m.status,(SELECT count(*) FROM ".PREFIX."tmovement) as countx FROM ".PREFIX."tmovement m INNER JOIN ".PREFIX."tdepartament d ON m.iddepartament=d.iddepartament WHERE (CAST(m.idmovement as CHAR) LIKE '%$search%' OR d.name LIKE '%$search%' OR CAST(m.date_register as CHAR) LIKE '%$search%') AND m.type_movement='2' ORDER BY m.idmovement DESC LIMIT $length OFFSET $start ");
		}
		$d["data"]= [];$d["recordsFiltered"] = 0;$d["recordsTotal"] = 0;
		foreach ($this->db->execute() as $key => $val) {
			$d["data"][$key]["idmovement"] = $val["idmovement"];
			$d["data"][$key]["name"] = $val["name"];
			$d["data"][$key]["btn"] = $this->permission->getpermission($val["idmovement"],$val["status"]);
			$d["recordsFiltered"] = $val["countx"];
			$d["recordsTotal"]++;
		}
		$d["draw"] = $draw;
		return $d;
	}
	public function dependencies(){
		$this->db->prepare("SELECT idtype_entry,name FROM ".PREFIX."ttype_entry WHERE status='1';");
		$dependencies["type_entrys"] = $this->db->execute();
		$this->db->prepare("SELECT iddepartament,name FROM ".PREFIX."tdepartament WHERE status='1';");
		$dependencies["departaments"] = $this->db->execute();
		$this->db->prepare("SELECT a.idarticle,CONCAT(a.name,'-',m.name,'-',b.name,' En existencia: ',a.amount) as name,a.amount FROM ".PREFIX."tarticle a INNER JOIN ".PREFIX."tmodel m ON a.idmodel=m.idmodel INNER JOIN ".PREFIX."tbrand b ON m.idbrand=b.idbrand INNER JOIN ".PREFIX."ttype_article ta ON a.idtype_article=ta.idtype_article WHERE a.status='1';");
		$dependencies["articles"] = $this->db->execute();
		$this->db->prepare("SELECT a.idarticle,CONCAT(a.name,'-',m.name,'-',b.name,' | En existencia: ',a.amount) as name,ma.amount FROM ".PREFIX."tdmovement_article ma INNER JOIN ".PREFIX."tarticle a ON ma.idarticle=a.idarticle INNER JOIN ".PREFIX."tmodel m ON a.idmodel=m.idmodel INNER JOIN ".PREFIX."tbrand b ON m.idbrand=b.idbrand INNER JOIN ".PREFIX."ttype_article ta ON a.idtype_article=ta.idtype_article WHERE ma.idmovement=?;");
		$this->db->execute(array($this->idmovement));
		$dependencies["movement_articles"] = $this->db->fetchAll();
		$dependencies["add"] = $this->permission->getpermissionadd();
		return $dependencies;
	}
	public function add(){
		//$this->db->prepare("INSERT INTO ".PREFIX."tmovement (type_movement,idtype_entry,idprovider,iddepartament,iduser,status,date_register) VALUES (?,?,?,?,?,'1',?)");
		$this->db->prepare("INSERT INTO ".PREFIX."tmovement (type_movement,idtype_entry,idprovider,iddepartament,iduser,status,date_register, number_billing_request,date_billing_request) VALUES (?,?,?,?,?,'1',?, ?, ?)");	
		return $this->db->execute(array($this->type_movement,$this->idtype_entry,$this->idprovider,$this->iddepartament,$_SESSION["iduser"],$this->date_register, $this->number_billing_request, $this->date_billing_request));
	}
	public function add_two($add,$type){
		$this->db->prepare("SELECT idmovement FROM ".PREFIX."tmovement ORDER BY idmovement DESC LIMIT 1 OFFSET 0;");
		$this->db->execute();
		$d=$this->db->fetchAll();
		$this->db->prepare("INSERT INTO ".PREFIX."tdmovement_article (idmovement,code,idarticle,amount) VALUES (?,?,?,?)");
		$this->db->execute(array(($add==1? $d[0]["idmovement"] : $this->idmovement),$this->code,$this->idarticle,$this->amount));
		if($type==1){
			$this->db->prepare("UPDATE ".PREFIX."tarticle SET amount=(amount+?) WHERE idarticle=?");
		}else{
			$this->db->prepare("UPDATE ".PREFIX."tarticle SET amount=(amount-?) WHERE idarticle=?");
		}
		return $this->db->execute(array($this->amount,$this->idarticle));
	}
	public function query($type){
		if($type==1){
			$this->db->prepare("SELECT *,CONCAT(prv.identification_card,' ',prv.name) as full_provider, CONCAT(p.name_one, ' ', p.last_name_one,' / ', d.name) as responsable
			FROM ".PREFIX."tmovement m 
			INNER JOIN ".PREFIX."tprovider prv ON m.idprovider=prv.idprovider
			INNER JOIN ".PREFIX."tuser u ON m.iduser=u.iduser
			INNER JOIN ".PREFIX."tperson p ON p.idperson=u.idperson
			INNER JOIN ".PREFIX."tdepartament d ON p.idperson = d.idperson
			WHERE idmovement=?;
			");
		}else{
			//$this->db->prepare("SELECT *,d.name as full_provider FROM ".PREFIX."tmovement m INNER JOIN ".PREFIX."tdepartament d ON m.iddepartament=d.iddepartament WHERE idmovement=? ;");
			$this->db->prepare("SELECT m.idmovement, rc.code, r.date_created, d.iddepartament, m.date_register, m.observation, d.name as full_provider, CONCAT(p.name_one,' ',p.last_name_one,' / ',d.name) as applicant
			FROM ".PREFIX."tmovement m 
			INNER JOIN ".PREFIX."tuser u ON m.iduser = u.iduser
			INNER JOIN ".PREFIX."tperson p ON u.idperson = p.idperson
			INNER JOIN ".PREFIX."tdepartament d ON p.idperson = d.idperson
			INNER JOIN ".PREFIX."trequest r ON u.iduser = r.iduser
			INNER JOIN ".PREFIX."trequest_code rc ON r.idrequest = rc.idrequest
			INNER JOIN ".PREFIX."tdrequest dr ON r.idrequest = dr.idrequest
			WHERE m.idmovement=?;");
		}
		$data=$this->db->execute(array($this->idmovement));
		foreach ($data as $val) { $d=$val; }
		return $d;
	}
	public function edit(){
		$this->db->prepare("UPDATE ".PREFIX."tmovement SET type_movement=?,idtype_entry=?,idprovider=?,iddepartament=?,date_register=? WHERE idmovement=?;");
		return $this->db->execute(array($this->type_movement,$this->idtype_entry,$this->idprovider,$this->iddepartament,$this->date_register,$this->idmovement));
	}
	public function delete_ma(){
		$this->db->prepare("DELETE FROM ".PREFIX."tdmovement_article WHERE idmovement=?;");
		return $this->db->execute(array($this->idmovement));
	}
	public function delete(){
		$this->db->prepare("DELETE FROM ".PREFIX."tmovement WHERE idmovement=?;");
		return $this->db->execute(array($this->idmovement));
	}
	public function status($num){
		$this->db->prepare("UPDATE ".PREFIX."tmovement SET status=? WHERE idmovement=?;");
		return $this->db->execute(array($num,$this->idmovement));
	}
	public function pdf($type_movement){
		/* $this->db->prepare("SELECT * FROM ".PREFIX."tmovement ORDER BY 1 DESC;");
		$data=$this->db->execute();
		foreach ($data as $val) { $d[]=$val; }
		return $d; */
		$this->db->prepare("SELECT m.idmovement,date_register,CONCAT(p.name_one,' ',p.last_name_one,' / ',d.name) as responsable
			FROM ".PREFIX."tmovement m 
			INNER JOIN ".PREFIX."tuser u ON m.iduser = u.iduser
			INNER JOIN ".PREFIX."tperson p ON u.idperson = p.idperson
			INNER JOIN ".PREFIX."tdepartament d ON p.idperson = d.idperson
			WHERE m.type_movement=?;");

		foreach ($this->db->execute(array($type_movement)) as $key => $val) {
			$d[$key]["idmovement"] = $val["idmovement"];
			$d[$key]["code"] = $val["code"];
			$d[$key]["date_register"] = $val["date_register"];
			$d[$key]["responsable"] = $val["responsable"];
			$d[$key]['articles'] = [];

			$this->db->prepare("SELECT a.idarticle,CONCAT(a.name,'-',m.name,'-',b.name,' (',ma.amount,')') as article
			FROM ".PREFIX."tmovement mm 
			INNER JOIN ".PREFIX."tdmovement_article ma ON mm.idmovement = ma.idmovement 
			INNER JOIN ".PREFIX."tarticle a ON ma.idarticle=a.idarticle 
			INNER JOIN ".PREFIX."tmodel m ON a.idmodel=m.idmodel 
			INNER JOIN ".PREFIX."tbrand b ON m.idbrand=b.idbrand 
			WHERE mm.idmovement=?;");

			foreach ($this->db->execute(array($d[$key]["idmovement"])) as $key2 => $val2){
				$d[$key]['articles'][] =  $val2['article'];
			}
			$d[$key]['articles'] = implode("\n", $d[$key]['articles']);
		}
		return $d;
	}
}
?>