<?php
namespace Models;
class modelModel{
	private $db,$permission;
	public $idmodel,$idbrand,$name,$status;
	public function __construct(){
		$this->db = new \core\ameliaBD;
		$this->permission = new \models\permissionModel;
	}
	public function listt($draw,$search,$start,$length){
		$start = (empty($start))? 0 : $start;
		$length = (empty($length))? 10 : $length;
		$this->db->prepare("SELECT *,(SELECT count(*) FROM ".PREFIX."tmodel) as countx FROM ".PREFIX."tmodel WHERE CAST(idmodel as CHAR) LIKE '%$search%' OR name LIKE '%$search%' ORDER BY idmodel DESC LIMIT $length OFFSET $start ");
		$d["data"]= [];$d["recordsFiltered"] = 0;$d["recordsTotal"] = 0;
		foreach ($this->db->execute() as $key => $val) {
			$d["data"][$key]["idmodel"] = $val["idmodel"];
			$d["data"][$key]["name"] = $val["name"];
			$d["data"][$key]["btn"] = $this->permission->getpermission($val["idmodel"],$val["status"]);
			$d["recordsFiltered"] = $val["countx"];
			$d["recordsTotal"]++;
		}
		$d["draw"] = $draw;
		return $d;
	}
	public function dependencies(){
		$this->db->prepare("SELECT idbrand,name FROM ".PREFIX."tbrand WHERE status='1';");
		$dependencies["brands"] = $this->db->execute();
		$dependencies["add"] = $this->permission->getpermissionadd();
		return $dependencies;
	}
	public function add(){
		$this->db->prepare("INSERT INTO ".PREFIX."tmodel (idbrand,name,status) VALUES (?,?,'1');");
		return $this->db->execute(array($this->idbrand,$this->name));
	}
	public function query(){
		$this->db->prepare("SELECT m.idmodel,b.idbrand,b.name as brand,m.name,m.status FROM ".PREFIX."tmodel m INNER JOIN ".PREFIX."tbrand b ON m.idbrand=b.idbrand WHERE idmodel=? ;");
		$data=$this->db->execute(array($this->idmodel));
		foreach ($data as $val) { $d=$val; }
		return $d;
	}
	public function edit(){
		$this->db->prepare("UPDATE ".PREFIX."tmodel SET idbrand=?,name=? WHERE idmodel=?;");
		return $this->db->execute(array($this->idbrand,$this->name,$this->idmodel));
	}
	public function delete(){
		$this->db->prepare("DELETE FROM ".PREFIX."tmodel WHERE idmodel=?;");
		return $this->db->execute(array($this->idmodel));
	}
	public function status($num){
		$this->db->prepare("UPDATE ".PREFIX."tmodel SET status=? WHERE idmodel=?;");
		return $this->db->execute(array($num,$this->idmodel));
	}
	public function exist($value){
		$this->db->prepare("SELECT lower(name) as namex FROM ".PREFIX."tmodel WHERE lower(name) = '$value' AND status='1';");
		$data=$this->db->execute();
		foreach ($data as $val) { $d[]=$val; }
		return $d;
	}
	public function search($value){
		$this->db->prepare("SELECT m.idmodel,CONCAT(m.name,' - ',b.name) as name FROM ".PREFIX."tmodel m INNER JOIN ".PREFIX."tbrand b ON m.idbrand=b.idbrand WHERE lower(m.name) LIKE lower('%$value%') AND m.status='1';");
		$data=$this->db->execute();
		foreach ($data as $val) { $d[]=$val; }
		return $d;
	}
	public function pdf(){
		$this->db->prepare("SELECT * FROM ".PREFIX."tmodel ORDER BY 1 DESC;");
		$data=$this->db->execute();
		foreach ($data as $val) { $d[]=$val; }
		return $d;
	}
}
?>