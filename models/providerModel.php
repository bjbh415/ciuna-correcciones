<?php
namespace Models;
class providerModel{
	private $db,$permission;
	public $idprovider,$identification_card,$name,$email,$idaddress,$address,$phone_one,$phone_two,$observation,$status;
	public function __construct(){
		$this->db = new \core\ameliaBD;
		$this->permission = new \models\permissionModel;
	}
	public function listt($draw,$search,$start,$length){
		$start = (empty($start))? 0 : $start;
		$length = (empty($length))? 10 : $length;
		$this->db->prepare("SELECT p.idprovider,p.identification_card,p.name,p.status,(SELECT count(*) FROM ".PREFIX."tprovider) as countx FROM ".PREFIX."tprovider p WHERE CAST(p.idprovider as CHAR) LIKE '%$search%' OR p.identification_card LIKE '%$search%' OR p.name LIKE '%$search%' ORDER BY idprovider DESC LIMIT $length OFFSET $start ");
		$d["data"]= [];$d["recordsFiltered"] = 0;$d["recordsTotal"] = 0;
		foreach ($this->db->execute() as $key => $val) {
			$d["data"][$key]["idprovider"] = $val["idprovider"];
			$d["data"][$key]["identification_card"] = $val["identification_card"];
			$d["data"][$key]["name"] = $val["name"];
			$d["data"][$key]["btn"] = $this->permission->getpermission($val["idprovider"],$val["status"]);
			$d["recordsFiltered"] = $val["countx"];
			$d["recordsTotal"]++;
		}
		$d["draw"] = $draw;
		return $d;
	}
	public function dependencies(){
		$this->db->prepare("SELECT idcharge,name FROM ".PREFIX."tcharge WHERE status='1';");
		$dependencies["charges"] = $this->db->execute();
		$dependencies["add"] = $this->permission->getpermissionadd();
		return $dependencies;
	}
	public function add(){
		$this->db->prepare("INSERT INTO ".PREFIX."tprovider (identification_card,name,email,idaddress,address,phone_one,phone_two,observation,status) VALUES (?,?,?,?,?,?,?,?,'1');");
		return $this->db->execute(array($this->identification_card,$this->name,$this->email,$this->idaddress,$this->address,$this->phone_one,$this->phone_two,$this->observation));
	}
	public function query(){
		$this->db->prepare("SELECT p.idprovider,p.identification_card,p.name,p.email,p.idaddress,CONCAT(pa.name,' - ',m.name,' - ',s.name,' - ',c.name) as full_address,p.address,p.phone_one,p.phone_two,p.observation FROM ".PREFIX."tprovider p INNER JOIN ".PREFIX."taddress pa ON p.idaddress=pa.idaddress INNER JOIN ".PREFIX."taddress m ON pa.idfather=m.idaddress INNER JOIN ".PREFIX."taddress s ON m.idfather=s.idaddress INNER JOIN ".PREFIX."taddress c ON s.idfather=c.idaddress WHERE p.idprovider=? ;");
		$data=$this->db->execute(array($this->idprovider));
		foreach ($data as $val) { $d=$val; }
		return $d;
	}
	public function edit(){
		$this->db->prepare("UPDATE ".PREFIX."tprovider SET identification_card=?,name=?,email=?,idaddress=?,address=?,phone_one=?,phone_two=?,observation=? WHERE idprovider=?;");
		return $this->db->execute(array($this->identification_card,$this->name,$this->email,$this->idaddress,$this->address,$this->phone_one,$this->phone_two,$this->observation,$this->idprovider));
	}
	public function delete(){
		$this->db->prepare("DELETE FROM ".PREFIX."tuser WHERE idprovider=?;");
		$this->db->execute(array($this->idprovider));
		$this->db->prepare("DELETE FROM ".PREFIX."tprovider WHERE idprovider=?;");
		return $this->db->execute(array($this->idprovider));
	}
	public function status($num){
		$this->db->prepare("UPDATE ".PREFIX."tprovider SET status=? WHERE idprovider=?;");
		return $this->db->execute(array($num,$this->idprovider));
	}
	public function pdf(){
		$this->db->prepare("SELECT p.*, c.name as address FROM ".PREFIX."tprovider p INNER JOIN ".PREFIX."taddress c ON p.idaddress = c.idaddress ORDER BY 1 DESC;");
		$data=$this->db->execute();
		foreach ($data as $val) { $d[]=$val; }
		return $d;
	}
	public function search($value){
		$this->db->prepare("SELECT p.idprovider,CONCAT(p.identification_card,' - ',p.name) as full_provider FROM ".PREFIX."tprovider p WHERE (lower(p.identification_card) LIKE lower('%$value%') OR lower(p.name) LIKE lower('%$value%')) AND p.status='1';");
		$data=$this->db->execute();
		foreach ($data as $val) { $d[]=$val; }
		return $d;
	}
}
?>