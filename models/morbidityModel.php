<?php
namespace Models;
class morbidityModel{
	private $db,$permission;
	public $idmorbidity,$name,$status;
	public function __construct(){
		$this->db = new \core\ameliaBD;
		$this->permission = new \models\permissionModel;
	}
	public function listt($draw,$search,$start,$length){
		$start = (empty($start))? 0 : $start;
		$length = (empty($length))? 10 : $length;
		$this->db->prepare("SELECT *,(SELECT count(*) FROM ".PREFIX."tmorbidity) as countx FROM ".PREFIX."tmorbidity WHERE CAST(idmorbidity as CHAR) LIKE '%$search%' OR name LIKE '%$search%' ORDER BY idmorbidity DESC LIMIT $length OFFSET $start ");
		$d["data"]= [];$d["recordsFiltered"] = 0;$d["recordsTotal"] = 0;
		foreach ($this->db->execute() as $key => $val) {
			$d["data"][$key]["idmorbidity"] = $val["idmorbidity"];
			$d["data"][$key]["name"] = $val["name"];
			$d["data"][$key]["btn"] = $this->permission->getpermission($val["idmorbidity"],$val["status"]);
			$d["recordsFiltered"] = $val["countx"];
			$d["recordsTotal"]++;
		}
		$d["draw"] = $draw;
		return $d;
	}
	public function dependencies(){
		return $this->permission->getpermissionadd();
	}
	public function add(){
		$this->db->prepare("INSERT INTO ".PREFIX."tmorbidity (name,status) VALUES (?,'1');");
		return $this->db->execute(array($this->name));
	}
	public function query(){
		$this->db->prepare("SELECT * FROM ".PREFIX."tmorbidity WHERE idmorbidity=? ;");
		$data=$this->db->execute(array($this->idmorbidity));
		foreach ($data as $val) { $d=$val; }
		return $d;
	}
	public function edit(){
		$this->db->prepare("UPDATE ".PREFIX."tmorbidity SET name=? WHERE idmorbidity=?;");
		return $this->db->execute(array($this->name,$this->idmorbidity));
	}
	public function delete(){
		$this->db->prepare("DELETE FROM ".PREFIX."tmorbidity WHERE idmorbidity=?;");
		return $this->db->execute(array($this->idmorbidity));
	}
	public function status($num){
		$this->db->prepare("UPDATE ".PREFIX."tmorbidity SET status=? WHERE idmorbidity=?;");
		return $this->db->execute(array($num,$this->idmorbidity));
	}
	public function pdf(){
		$this->db->prepare("SELECT * FROM ".PREFIX."tmorbidity ORDER BY 1 DESC;");
		$data=$this->db->execute();
		foreach ($data as $val) { $d[]=$val; }
		return $d;
	}
}
?>