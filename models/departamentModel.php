<?php
namespace Models;
class departamentModel{
	private $db,$permission;
	public $iddepartament,$name,$status,$idperson;
	public function __construct(){
		$this->db = new \core\ameliaBD;
		$this->permission = new \models\permissionModel;
	}
	public function listt($draw,$search,$start,$length){
		$start = (empty($start))? 0 : $start;
		$length = (empty($length))? 10 : $length;
		$this->db->prepare("SELECT *,(SELECT count(*) FROM ".PREFIX."tdepartament) as countx FROM ".PREFIX."tdepartament WHERE CAST(iddepartament as CHAR) LIKE '%$search%' OR name LIKE '%$search%' ORDER BY iddepartament DESC LIMIT $length OFFSET $start ");
		$d["data"]= [];$d["recordsFiltered"] = 0;$d["recordsTotal"] = 0;
		foreach ($this->db->execute() as $key => $val) {
			$d["data"][$key]["iddepartament"] = $val["iddepartament"];
			$d["data"][$key]["name"] = $val["name"];
			$d["data"][$key]["btn"] = $this->permission->getpermission($val["iddepartament"],$val["status"]);
			$d["recordsFiltered"] = $val["countx"];
			$d["recordsTotal"]++;
		}
		$d["draw"] = $draw;
		return $d;
	}
	public function dependencies(){
		$this->db->prepare("SELECT idperson,CONCAT(name_one,' ',last_name_one) as name FROM ".PREFIX."tperson WHERE status='1';");
		$dependencies["persons"] = $this->db->execute();
		$dependencies["add"] = $this->permission->getpermissionadd();
		return $dependencies;
	}
	public function add(){
		$this->db->prepare("INSERT INTO ".PREFIX."tdepartament (name,status,idperson) VALUES (?,'1',?);");
		return $this->db->execute(array($this->name,$this->idperson));
	}
	public function query(){
		$this->db->prepare("SELECT * FROM ".PREFIX."tdepartament WHERE iddepartament=? ;");
		$data=$this->db->execute(array($this->iddepartament));
		foreach ($data as $val) { $d=$val; }
		return $d;
	}
	public function edit(){
		$this->db->prepare("UPDATE ".PREFIX."tdepartament SET name=?,idperson=? WHERE iddepartament=?;");
		return $this->db->execute(array($this->name,$this->idperson,$this->iddepartament));
	}
	public function delete(){
		$this->db->prepare("DELETE FROM ".PREFIX."tdepartament WHERE iddepartament=?;");
		return $this->db->execute(array($this->iddepartament));
	}
	public function status($num){
		$this->db->prepare("UPDATE ".PREFIX."tdepartament SET status=? WHERE iddepartament=?;");
		return $this->db->execute(array($num,$this->iddepartament));
	}
	public function exist($value){
		$this->db->prepare("SELECT lower(name) as namex FROM ".PREFIX."tdepartament WHERE lower(name) = lower('$value') AND status='1';");
		$data=$this->db->execute();
		foreach ($data as $val) { $d[]=$val; }
		return $d;
	}
	public function pdf(){
		$this->db->prepare("SELECT * FROM ".PREFIX."tdepartament ORDER BY 1 DESC;");
		$data=$this->db->execute();
		foreach ($data as $val) { $d[]=$val; }
		return $d;
	}
}
?>