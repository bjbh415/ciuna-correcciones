<?php
namespace Models;
class type_entryModel{
	private $db,$permission;
	public $idtype_entry,$name,$status;
	public function __construct(){
		$this->db = new \core\ameliaBD;
		$this->permission = new \models\permissionModel;
	}
	public function listt($draw,$search,$start,$length){
		$start = (empty($start))? 0 : $start;
		$length = (empty($length))? 10 : $length;
		$this->db->prepare("SELECT *,(SELECT count(*) FROM ".PREFIX."ttype_entry) as countx FROM ".PREFIX."ttype_entry WHERE CAST(idtype_entry as CHAR) LIKE '%$search%' OR name LIKE '%$search%' ORDER BY idtype_entry DESC LIMIT $length OFFSET $start ");
		$d["data"]= [];$d["recordsFiltered"] = 0;$d["recordsTotal"] = 0;
		foreach ($this->db->execute() as $key => $val) {
			$d["data"][$key]["idtype_entry"] = $val["idtype_entry"];
			$d["data"][$key]["name"] = $val["name"];
			$d["data"][$key]["btn"] = $this->permission->getpermission($val["idtype_entry"],$val["status"]);
			$d["recordsFiltered"] = $val["countx"];
			$d["recordsTotal"]++;
		}
		$d["draw"] = $draw;
		return $d;
	}
	public function dependencies(){
		return $this->permission->getpermissionadd();
	}
	public function add(){
		$this->db->prepare("INSERT INTO ".PREFIX."ttype_entry (name,status) VALUES (?,'1');");
		return $this->db->execute(array($this->name));
	}
	public function query(){
		$this->db->prepare("SELECT * FROM ".PREFIX."ttype_entry WHERE idtype_entry=? ;");
		$data['type_entry']=$this->db->execute(array($this->idtype_entry))->fetch();
		$this->db->prepare("SELECT m.idmovement, m.idtype_entry, CONCAT(p.identification_card,'-',p.name,' / ',m.date_register) as name, m.status, (SELECT count(*) FROM ".PREFIX."tmovement) as countx FROM ".PREFIX."tmovement m INNER JOIN ".PREFIX."tprovider p ON m.idprovider=p.idprovider WHERE m.type_movement='1' AND m.idtype_entry=? ORDER BY m.idmovement DESC; ");
		$data['entrys']=$this->db->execute(array($this->idtype_entry))->fetchAll();
		return $data;
	}
	public function edit(){
		$this->db->prepare("UPDATE ".PREFIX."ttype_entry SET name=? WHERE idtype_entry=?;");
		return $this->db->execute(array($this->name,$this->idtype_entry));
	}
	public function delete(){
		$this->db->prepare("DELETE FROM ".PREFIX."ttype_entry WHERE idtype_entry=?;");
		return $this->db->execute(array($this->idtype_entry));
	}
	public function status($num){
		$this->db->prepare("UPDATE ".PREFIX."ttype_entry SET status=? WHERE idtype_entry=?;");
		return $this->db->execute(array($num,$this->idtype_entry));
	}
	public function pdf(){
		$this->db->prepare("SELECT * FROM ".PREFIX."ttype_entry ORDER BY 1 DESC;");
		$data=$this->db->execute();
		foreach ($data as $val) { $d[]=$val; }
		return $d;
	}
}
?>