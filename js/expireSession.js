//Tiempo límite de inactividad 180000ms (3min)
const EXPIRY_TIME = 180000;

//Termina la sesión del usuario
function expireSession(){
  var getUrl = window.location;
  var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];

  $.dialog({title:'Sesión Expirada',content:'Lo sentimos, su sesión será cerrada por superar el tiempo límite de inactividad (3mín)'});
  setTimeout(function(){ window.location=baseUrl+"/logout"; }, 3000);
}

function start() {
	return setTimeout(function() { expireSession() },EXPIRY_TIME);
}

function reset(time) {
	//limpia el timeout para resetear el tiempo desde cero
	clearTimeout(time);
	console.log("reset")
	return setTimeout(function() { expireSession() },EXPIRY_TIME);
}

$(document).ready(()=>{
	var time = start();

	//pone todos los elementos hijos de body a la escucha de eventos
	$("body *").on("click mousemove keypress", function(){
		time = reset(time);
	})
	
})