<?php
	define('controller_base','homeController');
	define('version','ciunav1');
	define('url_base','http://localhost/ciuna/');
	define('url_api','http://192.168.1.103/ciuna/');
	/*define('DRIVER','mysql');
	define('HOST','localhost');
	define('PORT','3306');
	define('USER','root');
	define('PASSWORD','');*/
	define('DRIVER','pgsql');
	define('HOST','localhost');
	define('PORT','5432');
	define('USER','postgres');
	define('PASSWORD','1234');

	define('PREFIX','acms_');
	define('DB_TEST','ciuna_test');
	define('DB_PRODUCTION','ciuna_production');

	ini_set('max_execution_time','60');
	date_default_timezone_set('America/Caracas');
	require_once 'languages/es.php';
?>
