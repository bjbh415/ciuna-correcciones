<?php
namespace controllers;
class providerController{
	private $provider,$permission,$log_movement;
	public function __construct(){
		define("controller","provider");
		$this->provider = new \models\providerModel;
		$this->permission = new \models\permissionModel;
		$this->log_movement = new \models\log_movementModel;
	}
	public function index(){
		$this->log_movement->add($_SESSION["iduser"],3,14,log_movement_message_list);
		$this->permission->getpermission_action(array(1,2,3,4,5,7));
		$data["dependencies"] = $this->provider->dependencies();
		view("provider.php",1,$data);
	}
	public function data($id=""){
		$this->provider->idprovider=$id;
		$this->provider->identification_card=$_POST["rif"].$_POST["identification_card"];
		$this->provider->name=ucwords($_POST["name"]);
		$this->provider->idaddress=$_POST["idaddress"];
		$this->provider->address=$_POST["address"];
		$this->provider->email=$_POST["email"];
		$this->provider->phone_one=$_POST["phone_one"];
		$this->provider->phone_two=$_POST["phone_two"];
		$this->provider->observation=$_POST["observation"];
	}
	public function listt(){
		echo json_encode($this->provider->listt($_POST["draw"],$_POST["search"]["value"],$_POST["start"],$_POST['length']));
	}
	public function add(){
		$this->permission->getpermission_action(1);
		if(isset($_POST["event"])){
			$this->data();
			$_SESSION["msj"] = ($this->provider->add())? add_success : add_error;
			//$this->log_movement->add($_SESSION["iduser"],1,14,$_SESSION["msj"],"{".id.":'',".person_idnationality.":'".$_POST["idnationality"]."',".person_idethnicity.":'".$_POST["idethnicity"]."',".person_charge.":'".$_POST["idcharge"]."',".person_identification_card.":'".$_POST["identification_card"]."',".person_image.":'".$_POST["image"]."',".person_name_one.":'".$_POST["name_one"]."',".person_name_two.":'".$_POST["name_two"]."',".person_last_name_one.":'".$_POST["last_name_one"]."',".person_last_name_two.":'".$_POST["last_name_two"]."',".person_sex.":'".$_POST["sex"]."',".person_email.":'".$_POST["email"]."',".person_birth_date.":'".$birth_date."',".person_birth_place.":'".$_POST["birth_place"]."',".person_idaddress.":'".$_POST["idaddress"]."',".person_address.":'".$_POST["address"]."',".person_phone_one.":'".$_POST["phone_one"]."',".person_phone_two.":'".$_POST["phone_two"]."'}");
			header("location: ".url_base.controller."/add");
			exit;
		}
		$data["dependencies"] = $this->provider->dependencies();
		view("provider.php",1,$data);	
	}
	public function query($id){
		$this->permission->getpermission_action(array(2,3));
		$this->provider->idprovider=$id;
		$data["d"] = $this->provider->query();
		$data["dependencies"] = $this->provider->dependencies();
		//$this->log_movement->add($_SESSION["iduser"],3,14,query,"{".id.":'".$id."'}");
		view("provider.php",1,$data);	
	}
	public function edit($id){
		$this->permission->getpermission_action(2);
		if(isset($_POST["event"])){
			$this->data($id);
			$_SESSION["msj"] = ($this->provider->edit())? edit_success : edit_error;
			//$this->log_movement->add($_SESSION["iduser"],2,14,$_SESSION["msj"],"{".id.":'".$id."',".person_idnationality.":'".$_POST["idnationality"]."',".person_idethnicity.":'".$_POST["idethnicity"]."',".person_charge.":'".$_POST["idcharge"]."',".person_identification_card.":'".$_POST["identification_card"]."',".person_image.":'".$_POST["image"]."',".person_name_one.":'".$_POST["name_one"]."',".person_name_two.":'".$_POST["name_two"]."',".person_last_name_one.":'".$_POST["last_name_one"]."',".person_last_name_two.":'".$_POST["last_name_two"]."',".person_sex.":'".$_POST["sex"]."',".person_email.":'".$_POST["email"]."',".person_birth_date.":'".$birth_date."',".person_birth_place.":'".$_POST["birth_place"]."',".person_idaddress.":'".$_POST["idaddress"]."',".person_address.":'".$_POST["address"]."',".person_phone_one.":'".$_POST["phone_one"]."',".person_phone_two.":'".$_POST["phone_two"]."'}");
			header("location: ".url_base.controller."/edit/".$id);
			exit;
		}
		$this->query($id);
	}
	public function delete($id){
		$this->permission->getpermission_action(7);
		$this->provider->idprovider=$id;
		$_SESSION["msj"] = ($this->provider->delete())? delete_success : delete_error;
		//$this->log_movement->add($_SESSION["iduser"],7,14,$_SESSION["msj"],"{".id.":'".$id."'}");
		header("location: ".url_base.controller);
	}
	public function activate($id){
		$this->permission->getpermission_action(4);
		$this->provider->idprovider=$id;
		$_SESSION["msj"] = ($this->provider->status(1))? activate_success : activate_error;
		//$this->log_movement->add($_SESSION["iduser"],4,14,$_SESSION["msj"],"{".id.":'".$id."'}");
		header("location: ".url_base.controller);
	}
	public function deactivate($id){
		$this->permission->getpermission_action(5);
		$this->provider->idprovider=$id;
		$_SESSION["msj"] = ($this->provider->status(0))? deactivate_success : deactivate_error;
		//$this->log_movement->add($_SESSION["iduser"],5,14,$_SESSION["msj"],"{".id.":'".$id."'}");
		header("location: ".url_base.controller);
	}
	public function pdf(){
		$log_report = new \models\log_reportModel;
		$randon = str_shuffle("012345678900abcdefghijklmnopqrstuvwxyz");
		$log_report->add($_SESSION["iduser"],person,$randon);
		$organization = new \models\organizationModel;
		$org = $organization->query();
		$providers = $this->provider->pdf();
		require 'pdf/providerPdf.php';
	}
	public function search(){
		echo json_encode($this->provider->search($_POST["value"]));
	}
}
?>