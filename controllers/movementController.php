<?php
namespace controllers;
class movementController{
	private $movement,$permission,$log_movement;
	public function __construct(){
		define("controller",(routerCtrl=='entry'? "entry" : "assign"));
		$this->movement = new \models\movementModel;
		$this->permission = new \models\permissionModel;
		$this->log_movement = new \models\log_movementModel;
	}
	public function index(){
		$this->log_movement->add($_SESSION["iduser"],3,2,log_movement_message_list);
		$this->permission->getpermission_action(array(1,2,3,4,5,7));
		$data["dependencies"] = $this->movement->dependencies();
		view("movement.php",1,$data);
	}
	public function data($id=""){
		$this->movement->idmovement=$id;
        $this->movement->type_movement=$_POST["type_movement"];
        $this->movement->idtype_entry=(!empty($_POST["idtype_entry"])? $_POST["idtype_entry"] : 0);
        $this->movement->idprovider=(!empty($_POST["idprovider"])? $_POST["idprovider"] : 0);
        $this->movement->iddepartament=(!empty($_POST["iddepartament"])? $_POST["iddepartament"] : 0);
        $this->movement->date_register=$_POST["date_register"];
        $this->movement->number_billing_request=$_POST["number_billing_request"];
        $this->movement->date_billing_request=$_POST["date_billing_request"];
        $this->movement->observation=$_POST["observation"];
	}
	public function listt($type){
		echo json_encode($this->movement->listt($_POST["draw"],$_POST["search"]["value"],$_POST["start"],$_POST['length'],$type));
	}
	/*public function listt_assign(){
		echo json_encode($this->movement->listt($_POST["draw"],$_POST["search"]["value"],$_POST["start"],$_POST['length'],2));
	}*/
	public function add($type){
		$this->permission->getpermission_action(1);
		if(isset($_POST["event"])){
			$this->data();
            //$_SESSION["msj"] = ($this->movement->add())? add_success : add_error;
            if($this->movement->add()){
                foreach($_POST["idarticle"] as $key => $val){
                    $this->movement->code=$_POST["code"][$key];
                    $this->movement->idarticle=$val;
                    $this->movement->amount=$_POST["amount"][$key];
                    if($this->movement->add_two(1,$type)){
                        $_SESSION["msj"] = add_success;
                    }else{
                        $_SESSION["msj"] = add_error;
                        exit;
                    }
                }
            }else{
                $_SESSION["msj"] = add_error;
            }
			//$this->log_movement->add($_SESSION["iduser"],1,2,$_SESSION["msj"],"{".id.":'',".movement_name.":'".$_POST["name"]."'}");
			header("location: ".url_base.routerCtrl."/add");
			exit;
        }
        
        $data["dependencies"] = $this->movement->dependencies();
        //$data["type"] = $type;
		view("movement.php",1,$data);
	}
	public function query($id,$type){
		$this->permission->getpermission_action(array(2,3));
		$this->movement->idmovement=$id;
        $data["d"] = $this->movement->query($type);
        $data["dependencies"] = $this->movement->dependencies();
		$this->log_movement->add($_SESSION["iduser"],3,2,query,"{".id.":'".$id."'}");
		view("movement.php",1,$data);
	}
	public function edit($id,$type){
		$this->permission->getpermission_action(2);
		if(isset($_POST["event"])){
			$this->data($id);
            //$_SESSION["msj"] = ($this->movement->edit())? edit_success : edit_error;
            if($this->movement->edit()){
                $this->movement->delete_ma();
                foreach($_POST["idarticle"] as $key => $val){
                    $this->movement->code=$_POST["code"][$key];
                    $this->movement->idarticle=$val;
                    $this->movement->amount=$_POST["amount"][$key];
                    if($this->movement->add_two(0,$type)){
                        $_SESSION["msj"] = edit_success;
                    }else{
                        $_SESSION["msj"] = edit_error;
                        exit;
                    }
                }
            }else{
                $_SESSION["msj"] = edit_error;
            }
			$this->log_movement->add($_SESSION["iduser"],2,2,$_SESSION["msj"],"{".id.":'".$id."',".movement_name.":'".$_POST["name"]."'}");
			header("location: ".url_base.routerCtrl."/edit/".$id);
			exit;
        }
		$this->query($id,$type);
	}
	public function delete($id){
		$this->permission->getpermission_action(7);
		$this->movement->idmovement=$id;
		$_SESSION["msj"] = ($this->movement->delete())? delete_success : delete_error;
		$this->log_movement->add($_SESSION["iduser"],7,2,$_SESSION["msj"],"{".id.":'".$id."'}");
		header("location: ".url_base.routerCtrl);
	}
	public function activate($id){
		$this->permission->getpermission_action(4);
		$this->movement->idmovement=$id;
		$_SESSION["msj"] = ($this->movement->status(1))? activate_success : activate_error;
		$this->log_movement->add($_SESSION["iduser"],4,2,$_SESSION["msj"],"{".id.":'".$id."'}");
		header("location: ".url_base.routerCtrl);
	}
	public function deactivate($id){
		$this->permission->getpermission_action(5);
		$this->movement->idmovement=$id;
		$_SESSION["msj"] = ($this->movement->status(0))? deactivate_success : deactivate_error;
		$this->log_movement->add($_SESSION["iduser"],5,2,$_SESSION["msj"],"{".id.":'".$id."'}");
		header("location: ".url_base.routerCtrl);
	}
	public function pdf($type_movement){
		$log_report = new \models\log_reportModel;
		$randon = str_shuffle("012345678900abcdefghijklmnopqrstuvwxyz");
		$log_report->add($_SESSION["iduser"],movement,$randon);
		$organization = new \models\organizationModel;
		$org = $organization->query();
		$movements = $this->movement->pdf($type_movement);
		require 'pdf/movementPdf.php';
	}
}
?>
