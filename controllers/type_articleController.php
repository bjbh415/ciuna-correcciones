<?php
namespace controllers;
class type_articleController{
	private $batch,$permission,$log_movement;
	public function __construct(){
		define("controller","type_article");
		$this->type_article = new \models\type_articleModel;
		$this->permission = new \models\permissionModel;
		$this->log_movement = new \models\log_movementModel;
	}
	public function index(){
		$this->log_movement->add($_SESSION["iduser"],3,2,log_movement_message_list);
		echo $this->permission->getpermission_action(array(1,2,3,4,5,7));
		$data["dependencies"] = $this->type_article->dependencies();
		view("type_article.php",1,$data);
	}
	public function data($id=""){
		$this->type_article->idtype_article=$id;
		$this->type_article->name=ucwords($_POST["name"]);
		$this->type_article->code=ucwords($_POST["code"]);
		$this->type_article->idbatch=$_POST["idbatch"];
	}
	public function listt(){
		echo json_encode($this->type_article->listt($_POST["draw"],$_POST["search"]["value"],$_POST["start"],$_POST['length']));
	}
	public function add(){
		$this->permission->getpermission_action(1);
		if(isset($_POST["event"])){
			$this->data();
			$_SESSION["msj"] = ($this->type_article->add())? add_success : add_error;
			$this->log_movement->add($_SESSION["iduser"],1,2,$_SESSION["msj"],"{".id.":'',".type_article_name.":'".$_POST["name"]."'}");
			header("location: ".url_base.controller."/add");
			exit;
		}
		view("type_article.php",1);
	}
	public function query($id){
		$this->permission->getpermission_action(array(2,3));
		$this->type_article->idtype_article=$id;
		$data["d"] = $this->type_article->query();
		$this->log_movement->add($_SESSION["iduser"],3,2,query,"{".id.":'".$id."'}");
		view("type_article.php",1,$data);
	}
	public function edit($id){
		$this->permission->getpermission_action(2);
		if(isset($_POST["event"])){
			$this->data($id);
			$_SESSION["msj"] = ($this->type_article->edit())? edit_success : edit_error;
			$this->log_movement->add($_SESSION["iduser"],2,2,$_SESSION["msj"],"{".id.":'".$id."',".type_article_name.":'".$_POST["name"]."'}");
			header("location: ".url_base.controller."/edit/".$id);
			exit;
		}
		$this->query($id);
	}
	public function delete($id){
		$this->permission->getpermission_action(7);
		$this->type_article->idtype_article=$id;
		$_SESSION["msj"] = ($this->type_article->delete())? delete_success : delete_error;
		$this->log_movement->add($_SESSION["iduser"],7,2,$_SESSION["msj"],"{".id.":'".$id."'}");
		header("location: ".url_base.controller);
	}
	public function activate($id){
		$this->permission->getpermission_action(4);
		$this->type_article->idtype_article=$id;
		$_SESSION["msj"] = ($this->type_article->status(1))? activate_success : activate_error;
		$this->log_movement->add($_SESSION["iduser"],4,2,$_SESSION["msj"],"{".id.":'".$id."'}");
		header("location: ".url_base.controller);
	}
	public function deactivate($id){
		$this->permission->getpermission_action(5);
		$this->type_article->idtype_article=$id;
		$_SESSION["msj"] = ($this->type_article->status(0))? deactivate_success : deactivate_error;
		$this->log_movement->add($_SESSION["iduser"],5,2,$_SESSION["msj"],"{".id.":'".$id."'}");
		header("location: ".url_base.controller);
	}
	public function pdf(){
		$log_report = new \models\log_reportModel;
		$randon = str_shuffle("012345678900abcdefghijklmnopqrstuvwxyz");
		$log_report->add($_SESSION["iduser"],type_article,$randon);
		$organization = new \models\organizationModel;
		$org = $organization->query();
		$type_articles = $this->type_article->pdf();
		require 'pdf/type_articlePdf.php';
	}
	public function search(){
		echo json_encode($this->type_article->search($_POST["value"]));
	}
	public function exist(){
		echo json_encode($this->type_article->exist($_POST["value"]));
	}
}
?>
