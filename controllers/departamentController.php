<?php
namespace controllers;
class departamentController{
	private $departament,$permission,$log_movement;
	public function __construct(){
		define("controller","departament");
		$this->departament = new \models\departamentModel;
		$this->permission = new \models\permissionModel;
		$this->log_movement = new \models\log_movementModel;
	}
	public function index(){
		$this->log_movement->add($_SESSION["iduser"],3,2,log_movement_message_list);
		echo $this->permission->getpermission_action(array(1,2,3,4,5,7));
		$data["dependencies"]["add"] = $this->departament->dependencies();
		view("departament.php",1,$data);
	}
	public function data($id=""){
		$this->departament->iddepartament=$id;
		$this->departament->name=ucwords($_POST["name"]);
		$this->departament->idperson=ucwords($_POST["idperson"]);
	}
	public function listt(){
		echo json_encode($this->departament->listt($_POST["draw"],$_POST["search"]["value"],$_POST["start"],$_POST['length']));
	}
	public function add(){
		$this->permission->getpermission_action(1);
		if(isset($_POST["event"])){
			$this->data();
			$_SESSION["msj"] = ($this->departament->add())? add_success : add_error;
			$this->log_movement->add($_SESSION["iduser"],1,2,$_SESSION["msj"],"{".id.":'',".departament_name.":'".$_POST["name"]."'}");
			header("location: ".url_base.controller."/add");
			exit;
		}
		$data["dependencies"] = $this->departament->dependencies();
		view("departament.php",1,$data);
	}
	public function query($id){
		$this->permission->getpermission_action(array(2,3));
		$this->departament->iddepartament=$id;
		$data["d"] = $this->departament->query();
		$data["dependencies"] = $this->departament->dependencies();
		$this->log_movement->add($_SESSION["iduser"],3,2,query,"{".id.":'".$id."'}");
		view("departament.php",1,$data);
	}
	public function edit($id){
		$this->permission->getpermission_action(2);
		if(isset($_POST["event"])){
			$this->data($id);
			$_SESSION["msj"] = ($this->departament->edit())? edit_success : edit_error;
			$this->log_movement->add($_SESSION["iduser"],2,2,$_SESSION["msj"],"{".id.":'".$id."',".departament_name.":'".$_POST["name"]."'}");
			header("location: ".url_base.controller."/edit/".$id);
			exit;
		}
		$this->query($id);
	}
	public function delete($id){
		$this->permission->getpermission_action(7);
		$this->departament->iddepartament=$id;
		$_SESSION["msj"] = ($this->departament->delete())? delete_success : delete_error;
		$this->log_movement->add($_SESSION["iduser"],7,2,$_SESSION["msj"],"{".id.":'".$id."'}");
		header("location: ".url_base.controller);
	}
	public function activate($id){
		$this->permission->getpermission_action(4);
		$this->departament->iddepartament=$id;
		$_SESSION["msj"] = ($this->departament->status(1))? activate_success : activate_error;
		$this->log_movement->add($_SESSION["iduser"],4,2,$_SESSION["msj"],"{".id.":'".$id."'}");
		header("location: ".url_base.controller);
	}
	public function deactivate($id){
		$this->permission->getpermission_action(5);
		$this->departament->iddepartament=$id;
		$_SESSION["msj"] = ($this->departament->status(0))? deactivate_success : deactivate_error;
		$this->log_movement->add($_SESSION["iduser"],5,2,$_SESSION["msj"],"{".id.":'".$id."'}");
		header("location: ".url_base.controller);
	}
	public function pdf(){
		$log_report = new \models\log_reportModel;
		$randon = str_shuffle("012345678900abcdefghijklmnopqrstuvwxyz");
		$log_report->add($_SESSION["iduser"],departament,$randon);
		$organization = new \models\organizationModel;
		$org = $organization->query();
		$departaments = $this->departament->pdf();
		require 'pdf/departamentPdf.php';
	}
	public function exist(){
		echo json_encode($this->departament->exist($_POST["value"]));
	}
}
?>
