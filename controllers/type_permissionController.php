<?php
namespace controllers;
class type_permissionController{
	private $type_permission,$permission,$log_movement;
	public function __construct(){
		define("controller","type_permission");
		$this->type_permission = new \models\type_permissionModel;
		$this->permission = new \models\permissionModel;
		$this->log_movement = new \models\log_movementModel;
	}
	public function index(){
		$this->log_movement->add($_SESSION["iduser"],3,2,log_movement_message_list);
		echo $this->permission->getpermission_action(array(1,2,3,4,5,7));
		$data["dependencies"]["add"] = $this->type_permission->dependencies();
		view("type_permission.php",1,$data);
	}
	public function data($id=""){
		$this->type_permission->idtype_permission=$id;
		$this->type_permission->name=ucwords($_POST["name"]);
	}
	public function listt(){
		echo json_encode($this->type_permission->listt($_POST["draw"],$_POST["search"]["value"],$_POST["start"],$_POST['length']));
	}
	public function add(){
		$this->permission->getpermission_action(1);
		if(isset($_POST["event"])){
			$this->data();
			$_SESSION["msj"] = ($this->type_permission->add())? add_success : add_error;
			$this->log_movement->add($_SESSION["iduser"],1,2,$_SESSION["msj"],"{".id.":'',".type_permission_name.":'".$_POST["name"]."'}");
			header("location: ".url_base.controller."/add");
			exit;
		}
		view("type_permission.php",1);
	}
	public function query($id){
		$this->permission->getpermission_action(array(2,3));
		$this->type_permission->idtype_permission=$id;
		$data["d"] = $this->type_permission->query();
		$this->log_movement->add($_SESSION["iduser"],3,2,query,"{".id.":'".$id."'}");
		view("type_permission.php",1,$data);
	}
	public function edit($id){
		$this->permission->getpermission_action(2);
		if(isset($_POST["event"])){
			$this->data($id);
			$_SESSION["msj"] = ($this->type_permission->edit())? edit_success : edit_error;
			$this->log_movement->add($_SESSION["iduser"],2,2,$_SESSION["msj"],"{".id.":'".$id."',".type_permission_name.":'".$_POST["name"]."'}");
			header("location: ".url_base.controller."/edit/".$id);
			exit;
		}
		$this->query($id);
	}
	public function delete($id){
		$this->permission->getpermission_action(7);
		$this->type_permission->idtype_permission=$id;
		$_SESSION["msj"] = ($this->type_permission->delete())? delete_success : delete_error;
		$this->log_movement->add($_SESSION["iduser"],7,2,$_SESSION["msj"],"{".id.":'".$id."'}");
		header("location: ".url_base.controller);
	}
	public function activate($id){
		$this->permission->getpermission_action(4);
		$this->type_permission->idtype_permission=$id;
		$_SESSION["msj"] = ($this->type_permission->status(1))? activate_success : activate_error;
		$this->log_movement->add($_SESSION["iduser"],4,2,$_SESSION["msj"],"{".id.":'".$id."'}");
		header("location: ".url_base.controller);
	}
	public function deactivate($id){
		$this->permission->getpermission_action(5);
		$this->type_permission->idtype_permission=$id;
		$_SESSION["msj"] = ($this->type_permission->status(0))? deactivate_success : deactivate_error;
		$this->log_movement->add($_SESSION["iduser"],5,2,$_SESSION["msj"],"{".id.":'".$id."'}");
		header("location: ".url_base.controller);
	}
	public function pdf(){
		$log_report = new \models\log_reportModel;
		$randon = str_shuffle("012345678900abcdefghijklmnopqrstuvwxyz");
		$log_report->add($_SESSION["iduser"],type_permission,$randon);
		$organization = new \models\organizationModel;
		$org = $organization->query();
		$type_permissions = $this->type_permission->pdf();
		require 'pdf/type_permissionPdf.php';
	}
}
?>
