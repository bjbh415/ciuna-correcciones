<?php
namespace controllers;
class batchController{
	private $batch,$permission,$log_movement;
	public function __construct(){
		define("controller","batch");
		$this->batch = new \models\batchModel;
		$this->permission = new \models\permissionModel;
		$this->log_movement = new \models\log_movementModel;
	}
	public function index(){
		$this->log_movement->add($_SESSION["iduser"],3,2,log_movement_message_list);
		echo $this->permission->getpermission_action(array(1,2,3,4,5,7));
		$data["dependencies"]["add"] = $this->batch->dependencies();
		view("batch.php",1,$data);
	}
	public function data($id=""){
		$this->batch->idbatch=$id;
		$this->batch->name=ucwords($_POST["name"]);
		$this->batch->code=ucwords($_POST["code"]);
	}
	public function listt(){
		echo json_encode($this->batch->listt($_POST["draw"],$_POST["search"]["value"],$_POST["start"],$_POST['length']));
	}
	public function add(){
		$this->permission->getpermission_action(1);
		if(isset($_POST["event"])){
			$this->data();
			$_SESSION["msj"] = ($this->batch->add())? add_success : add_error;
			$this->log_movement->add($_SESSION["iduser"],1,2,$_SESSION["msj"],"{".id.":'',".batch_name.":'".$_POST["name"]."'}");
			header("location: ".url_base.controller."/add");
			exit;
		}
		view("batch.php",1);
	}
	public function query($id){
		$this->permission->getpermission_action(array(2,3));
		$this->batch->idbatch=$id;
		$data["d"] = $this->batch->query();
		$this->log_movement->add($_SESSION["iduser"],3,2,query,"{".id.":'".$id."'}");
		view("batch.php",1,$data);
	}
	public function edit($id){
		$this->permission->getpermission_action(2);
		if(isset($_POST["event"])){
			$this->data($id);
			$_SESSION["msj"] = ($this->batch->edit())? edit_success : edit_error;
			$this->log_movement->add($_SESSION["iduser"],2,2,$_SESSION["msj"],"{".id.":'".$id."',".batch_name.":'".$_POST["name"]."'}");
			header("location: ".url_base.controller."/edit/".$id);
			exit;
		}
		$this->query($id);
	}
	public function delete($id){
		$this->permission->getpermission_action(7);
		$this->batch->idbatch=$id;
		$_SESSION["msj"] = ($this->batch->delete())? delete_success : delete_error;
		$this->log_movement->add($_SESSION["iduser"],7,2,$_SESSION["msj"],"{".id.":'".$id."'}");
		header("location: ".url_base.controller);
	}
	public function activate($id){
		$this->permission->getpermission_action(4);
		$this->batch->idbatch=$id;
		$_SESSION["msj"] = ($this->batch->status(1))? activate_success : activate_error;
		$this->log_movement->add($_SESSION["iduser"],4,2,$_SESSION["msj"],"{".id.":'".$id."'}");
		header("location: ".url_base.controller);
	}
	public function deactivate($id){
		$this->permission->getpermission_action(5);
		$this->batch->idbatch=$id;
		$_SESSION["msj"] = ($this->batch->status(0))? deactivate_success : deactivate_error;
		$this->log_movement->add($_SESSION["iduser"],5,2,$_SESSION["msj"],"{".id.":'".$id."'}");
		header("location: ".url_base.controller);
	}
	public function pdf(){
		$log_report = new \models\log_reportModel;
		$randon = str_shuffle("012345678900abcdefghijklmnopqrstuvwxyz");
		//$log_report->add($_SESSION["iduser"],batch,$randon);
		$organization = new \models\organizationModel;
		$org = $organization->query();
		$batchs = $this->batch->pdf();
		require 'pdf/batchPdf.php';
	}
	public function search(){
		echo json_encode($this->batch->search($_POST["value"]));
	}
	public function exist(){
		echo json_encode($this->batch->exist($_POST["value"]));
	}
}
?>
