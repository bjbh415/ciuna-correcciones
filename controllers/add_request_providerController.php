<?php
namespace controllers;
class add_request_providerController{
	private $add_request,$permission,$log_movementModel;
	public function __construct(){
		define("controller",'add-request_provider');
		$this->add_request = new \models\request_providerModel;
		$this->permission = new \models\permissionModel;
		$this->log_movementModel = new \models\log_movementModel;
	}
	public function index(){
		$this->log_movementModel->add($_SESSION["iduser"],3,2,log_movementModel_message_list);
		$this->permission->getpermission_action(array(1,2,3,4,5,7));
		$data["dependencies"] = $this->add_request->dependencies();
		view("add_request_provider.php",1,$data);
	}
	public function data($id=""){
		$this->add_request->idrequest=$id;
        $this->add_request->type_add_request=$_POST["type_add_request"];
        $this->add_request->idtype_entry=(!empty($_POST["idtype_entry"])? $_POST["idtype_entry"] : 0);
        $this->add_request->idprovider=(!empty($_POST["idprovider"])? $_POST["idprovider"] : 0);
        $this->add_request->iddepartament=(!empty($_POST["iddepartament"])? $_POST["iddepartament"] : 0);
	}
	public function listt($type){
		echo json_encode($this->add_request->listt($_POST["draw"],$_POST["search"]["value"],$_POST["start"],$_POST['length'],$type));
	}
	public function add(){
		$this->permission->getpermission_action(1);
		if(isset($_POST["event"])){
			$this->data();
            if($this->add_request->add()){
                foreach($_POST["idarticle"] as $key => $val){
                    $this->add_request->idarticle=$val;
                    $this->add_request->amount=$_POST["amount"][$key];
                    if($this->add_request->add_two(1)){
                        $_SESSION["msj"] = add_success;
                    }else{
                        $_SESSION["msj"] = add_error;
                    }
                }
            }else{
                $_SESSION["msj"] = add_error;
            }			
			header("location: ".url_base.routerCtrl."/add");
			exit;
        }
        $data["dependencies"] = $this->add_request->dependencies();
		view("add_request_provider.php",1,$data);
	}
	public function query($id,$type){
		$this->permission->getpermission_action(array(2,3));
		$this->add_request->idrequest=$id;
        $data["d"] = $this->add_request->query($type);
        $data["dependencies"] = $this->add_request->dependencies();
		$this->log_movementModel->add($_SESSION["iduser"],3,2,query,"{".id.":'".$id."'}");
		view("add_request_provider.php",1,$data);
	}
	public function edit($id,$type){
		$this->permission->getpermission_action(2);
		if(isset($_POST["event"])){
			$this->data($id);
            //$_SESSION["msj"] = ($this->add_request->edit())? edit_success : edit_error;
            if($this->add_request->edit()){
                $this->add_request->delete_ma();
                foreach($_POST["idarticle"] as $key => $val){
                    $this->add_request->idarticle=$val;
                    $this->add_request->amount=$_POST["amount"][$key];
                    if($this->add_request->add_two(0)){
                        $_SESSION["msj"] = edit_success;
                    }else{
                        $_SESSION["msj"] = edit_error;
                        exit;
                    }
                }
            }else{
                $_SESSION["msj"] = edit_error;
            }
			$this->log_movementModel->add($_SESSION["iduser"],2,2,$_SESSION["msj"],"{".id.":'".$id."',".add_request_name.":'".$_POST["name"]."'}");
			header("location: ".url_base.routerCtrl."/edit/".$id);
			exit;
        }
		$this->query($id,$type);
	}
	public function delete($id){
		$this->permission->getpermission_action(7);
		$this->add_request->idrequest=$id;
		$_SESSION["msj"] = ($this->add_request->delete())? delete_success : delete_error;
		$this->log_movementModel->add($_SESSION["iduser"],7,2,$_SESSION["msj"],"{".id.":'".$id."'}");
		header("location: ".url_base.routerCtrl);
	}
	public function activate($id){
		$this->permission->getpermission_action(4);
		$this->add_request->idrequest=$id;
		$_SESSION["msj"] = ($this->add_request->status(1))? activate_success : activate_error;
		$this->log_movementModel->add($_SESSION["iduser"],4,2,$_SESSION["msj"],"{".id.":'".$id."'}");
		header("location: ".url_base.routerCtrl);
	}
	public function deactivate($id){
		$this->permission->getpermission_action(5);
		$this->add_request->idrequest=$id;
		$_SESSION["msj"] = ($this->add_request->status(0))? deactivate_success : deactivate_error;
		$this->log_movementModel->add($_SESSION["iduser"],5,2,$_SESSION["msj"],"{".id.":'".$id."'}");
		header("location: ".url_base.routerCtrl);
	}
	public function pdf(){
		$log_report = new \models\log_reportModel;
		$randon = str_shuffle("012345678900abcdefghijklmnopqrstuvwxyz");
		$log_report->add($_SESSION["iduser"],add_request,$randon);
		$organization = new \models\organizationModel;
		$org = $organization->query();
		$add_requests = $this->add_request->pdf();
		require 'pdf/request_providerPdf.php';
	}
}
?>
