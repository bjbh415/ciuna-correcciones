<?php
namespace controllers;
class modelController{
	private $model,$permission,$log_movement;
	public function __construct(){
		define("controller","model");
		$this->model = new \models\modelModel;
		$this->permission = new \models\permissionModel;
		$this->log_movement = new \models\log_movementModel;
	}
	public function index(){
		$this->log_movement->add($_SESSION["iduser"],3,2,log_movement_message_list);
		echo $this->permission->getpermission_action(array(1,2,3,4,5,7));
		$data["dependencies"]["add"] = $this->model->dependencies();
		view("model.php",1,$data);
	}
	public function data($id=""){
		$this->model->idmodel=$id;
		$this->model->name=ucwords($_POST["name"]);
		$this->model->idbrand=$_POST["idbrand"];
	}
	public function listt(){
		echo json_encode($this->model->listt($_POST["draw"],$_POST["search"]["value"],$_POST["start"],$_POST['length']));
	}
	public function add(){
		$this->permission->getpermission_action(1);
		if(isset($_POST["event"])){
			$this->data();
			$_SESSION["msj"] = ($this->model->add())? add_success : add_error;
			$this->log_movement->add($_SESSION["iduser"],1,2,$_SESSION["msj"],"{".id.":'',".model_name.":'".$_POST["name"]."'}");
			header("location: ".url_base.controller."/add");
			exit;
		}
		$data["dependencies"] = $this->model->dependencies();
		view("model.php",1,$data);
	}
	public function query($id){
		$this->permission->getpermission_action(array(2,3));
		$this->model->idmodel=$id;
		$data["d"] = $this->model->query();
		$data["dependencies"] = $this->model->dependencies();
		$this->log_movement->add($_SESSION["iduser"],3,2,query,"{".id.":'".$id."'}");
		view("model.php",1,$data);
	}
	public function edit($id){
		$this->permission->getpermission_action(2);
		if(isset($_POST["event"])){
			$this->data($id);
			$_SESSION["msj"] = ($this->model->edit())? edit_success : edit_error;
			$this->log_movement->add($_SESSION["iduser"],2,2,$_SESSION["msj"],"{".id.":'".$id."',".model_name.":'".$_POST["name"]."'}");
			header("location: ".url_base.controller."/edit/".$id);
			exit;
		}
		$this->query($id);
	}
	public function delete($id){
		$this->permission->getpermission_action(7);
		$this->model->idmodel=$id;
		$_SESSION["msj"] = ($this->model->delete())? delete_success : delete_error;
		$this->log_movement->add($_SESSION["iduser"],7,2,$_SESSION["msj"],"{".id.":'".$id."'}");
		header("location: ".url_base.controller);
	}
	public function activate($id){
		$this->permission->getpermission_action(4);
		$this->model->idmodel=$id;
		$_SESSION["msj"] = ($this->model->status(1))? activate_success : activate_error;
		$this->log_movement->add($_SESSION["iduser"],4,2,$_SESSION["msj"],"{".id.":'".$id."'}");
		header("location: ".url_base.controller);
	}
	public function deactivate($id){
		$this->permission->getpermission_action(5);
		$this->model->idmodel=$id;
		$_SESSION["msj"] = ($this->model->status(0))? deactivate_success : deactivate_error;
		$this->log_movement->add($_SESSION["iduser"],5,2,$_SESSION["msj"],"{".id.":'".$id."'}");
		header("location: ".url_base.controller);
	}
	public function pdf(){
		$log_report = new \models\log_reportModel;
		$randon = str_shuffle("012345678900abcdefghijklmnopqrstuvwxyz");
		$log_report->add($_SESSION["iduser"],model,$randon);
		$organization = new \models\organizationModel;
		$org = $organization->query();
		$models = $this->model->pdf();
		require 'pdf/modelPdf.php';
	}
	public function search(){
		echo json_encode($this->model->search($_POST["value"]));
	}
	public function exist(){
		echo json_encode($this->model->exist($_POST["value"]));
	}
}
?>
