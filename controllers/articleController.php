<?php
namespace controllers;
class articleController{
	private $article,$permission,$log_movement;
	public function __construct(){
		define("controller","article");
		$this->article = new \models\articleModel;
		$this->permission = new \models\permissionModel;
		$this->log_movement = new \models\log_movementModel;
	}
	public function index(){
		$this->log_movement->add($_SESSION["iduser"],3,2,log_movement_message_list);
		echo $this->permission->getpermission_action(array(1,2,3,4,5,7));
		$data["dependencies"] = $this->article->dependencies();
		view("article.php",1,$data);
	}
	public function data($id=""){
		$this->article->idarticle=$id;
		$this->article->idtype_article=$_POST["idtype_article"];
		$this->article->idmodel=$_POST["idmodel"];
		$this->article->idmeasurement=$_POST["idmeasurement"];
		$this->article->name=ucwords($_POST["name"]);
		//$this->article->amount=$_POST["amount"];
		$this->article->stockmin=$_POST["stockmin"];
		$this->article->stockmax=$_POST["stockmax"];
	}
	public function listt(){
		echo json_encode($this->article->listt($_POST["draw"],$_POST["search"]["value"],$_POST["start"],$_POST['length']));
	}
	public function add(){
		$this->permission->getpermission_action(1);
		if(isset($_POST["event"])){
			$this->data();
			$_SESSION["msj"] = ($this->article->add())? add_success : add_error;
			$this->log_movement->add($_SESSION["iduser"],1,2,$_SESSION["msj"],"{".id.":'',".article_name.":'".$_POST["name"]."'}");
			header("location: ".url_base.controller."/add");
			exit;
		}
		$data["dependencies"] = $this->article->dependencies();
		view("article.php",1,$data);
	}
	public function query($id){
		$this->permission->getpermission_action(array(2,3));
		$this->article->idarticle=$id;
		$data["d"] = $this->article->query();
		$this->log_movement->add($_SESSION["iduser"],3,2,query,"{".id.":'".$id."'}");
		$data["dependencies"] = $this->article->dependencies();
		view("article.php",1,$data);
	}
	public function edit($id){
		$this->permission->getpermission_action(2);
		if(isset($_POST["event"])){
			$this->data($id);
			$_SESSION["msj"] = ($this->article->edit())? edit_success : edit_error;
			$this->log_movement->add($_SESSION["iduser"],2,2,$_SESSION["msj"],"{".id.":'".$id."',".article_name.":'".$_POST["name"]."'}");
			header("location: ".url_base.controller."/edit/".$id);
			exit;
		}
		$this->query($id);
	}
	public function delete($id){
		$this->permission->getpermission_action(7);
		$this->article->idarticle=$id;
		$_SESSION["msj"] = ($this->article->delete())? delete_success : delete_error;
		$this->log_movement->add($_SESSION["iduser"],7,2,$_SESSION["msj"],"{".id.":'".$id."'}");
		header("location: ".url_base.controller);
	}
	public function activate($id){
		$this->permission->getpermission_action(4);
		$this->article->idarticle=$id;
		$_SESSION["msj"] = ($this->article->status(1))? activate_success : activate_error;
		$this->log_movement->add($_SESSION["iduser"],4,2,$_SESSION["msj"],"{".id.":'".$id."'}");
		header("location: ".url_base.controller);
	}
	public function deactivate($id){
		$this->permission->getpermission_action(5);
		$this->article->idarticle=$id;
		$_SESSION["msj"] = ($this->article->status(0))? deactivate_success : deactivate_error;
		$this->log_movement->add($_SESSION["iduser"],5,2,$_SESSION["msj"],"{".id.":'".$id."'}");
		header("location: ".url_base.controller);
	}
	public function exist(){
		echo json_encode($this->article->exist($_POST["value"]));
	}
	public function search(){
		echo json_encode($this->article->search($_POST["value"]));
	}
	public function pdf(){
		$log_report = new \models\log_reportModel;
		$randon = str_shuffle("012345678900abcdefghijklmnopqrstuvwxyz");
		$log_report->add($_SESSION["iduser"],movement,$randon);
		$organization = new \models\organizationModel;
		$org = $organization->query();
		$articles = $this->article->pdf();
		require 'pdf/articlePdf.php';
	}
}
?>

