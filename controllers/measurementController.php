<?php
namespace controllers;
class measurementController{
	private $measurement,$permission,$log_movement;
	public function __construct(){
		define("controller","measurement");
		$this->measurement = new \models\measurementModel;
		$this->permission = new \models\permissionModel;
		$this->log_movement = new \models\log_movementModel;
	}
	public function index(){
		$this->log_movement->add($_SESSION["iduser"],3,2,log_movement_message_list);
		echo $this->permission->getpermission_action(array(1,2,3,4,5,7));
		$data["dependencies"]["add"] = $this->measurement->dependencies();
		view("measurement.php",1,$data);
	}
	public function data($id=""){
		$this->measurement->idmeasurement=$id;
		$this->measurement->name=ucwords($_POST["name"]);
	}
	public function listt(){
		echo json_encode($this->measurement->listt($_POST["draw"],$_POST["search"]["value"],$_POST["start"],$_POST['length']));
	}
	public function add(){
		$this->permission->getpermission_action(1);
		if(isset($_POST["event"])){
			$this->data();
			$_SESSION["msj"] = ($this->measurement->add())? add_success : add_error;
			$this->log_movement->add($_SESSION["iduser"],1,2,$_SESSION["msj"],"{".id.":'',".measurement_name.":'".$_POST["name"]."'}");
			header("location: ".url_base.controller."/add");
			exit;
		}
		view("measurement.php",1);
	}
	public function query($id){
		$this->permission->getpermission_action(array(2,3));
		$this->measurement->idmeasurement=$id;
		$data["d"] = $this->measurement->query();
		$this->log_movement->add($_SESSION["iduser"],3,2,query,"{".id.":'".$id."'}");
		view("measurement.php",1,$data);
	}
	public function edit($id){
		$this->permission->getpermission_action(2);
		if(isset($_POST["event"])){
			$this->data($id);
			$_SESSION["msj"] = ($this->measurement->edit())? edit_success : edit_error;
			$this->log_movement->add($_SESSION["iduser"],2,2,$_SESSION["msj"],"{".id.":'".$id."',".measurement_name.":'".$_POST["name"]."'}");
			header("location: ".url_base.controller."/edit/".$id);
			exit;
		}
		$this->query($id);
	}
	public function delete($id){
		$this->permission->getpermission_action(7);
		$this->measurement->idmeasurement=$id;
		$_SESSION["msj"] = ($this->measurement->delete())? delete_success : delete_error;
		$this->log_movement->add($_SESSION["iduser"],7,2,$_SESSION["msj"],"{".id.":'".$id."'}");
		header("location: ".url_base.controller);
	}
	public function activate($id){
		$this->permission->getpermission_action(4);
		$this->measurement->idmeasurement=$id;
		$_SESSION["msj"] = ($this->measurement->status(1))? activate_success : activate_error;
		$this->log_movement->add($_SESSION["iduser"],4,2,$_SESSION["msj"],"{".id.":'".$id."'}");
		header("location: ".url_base.controller);
	}
	public function deactivate($id){
		$this->permission->getpermission_action(5);
		$this->measurement->idmeasurement=$id;
		$_SESSION["msj"] = ($this->measurement->status(0))? deactivate_success : deactivate_error;
		$this->log_movement->add($_SESSION["iduser"],5,2,$_SESSION["msj"],"{".id.":'".$id."'}");
		header("location: ".url_base.controller);
	}
	public function pdf(){
		$log_report = new \models\log_reportModel;
		$randon = str_shuffle("012345678900abcdefghijklmnopqrstuvwxyz");
		$log_report->add($_SESSION["iduser"],measurement,$randon);
		$organization = new \models\organizationModel;
		$org = $organization->query();
		$measurements = $this->measurement->pdf();
		require 'pdf/measurementPdf.php';
	}
	public function exist(){
		echo json_encode($this->measurement->exist($_POST["value"]));
	}
}
?>
